﻿package pages{
	
	import flash.display.MovieClip;
	
	
	public class GPhoto extends MovieClip {
		
		public var originalX:Number;
		public var originalY:Number;
		public var originalRotation:Number;
		public var originalWidth:Number;
		public var originalHeight:Number;
		public var defaultImg:MovieClip;
		
				
		public function GPhoto() {
			// constructor code
		}
		
		public function resetPhoto():void{
			
			
		}
		
		public function hideDefImage():void{
			trace("hideDefImage()");
			
			defaultImg.visible = false;
			
		}
		
	}
}
