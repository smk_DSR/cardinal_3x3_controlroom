﻿package pages
{
	import com.gaiaframework.templates.AbstractPage;
	import com.gaiaframework.events.*;
	import com.gaiaframework.debug.*;
	import com.gaiaframework.api.*;
	import fl.video.*;
	import flash.display.*;
	import flash.events.*;
	import com.greensock.*;
	import com.greensock.easing.*;
	
	public class OverviewPage extends AbstractPage
	{	
	
		//MovieClip Declarations
		public var display_mc:MovieClip;	
		public var bg_display_shape:MovieClip;
		public var annealing_lehr_btn:Sprite;
		public var tinbath_btn:Sprite;
		public var furnace_btn:Sprite;
		public var batch_house_btn:Sprite;		
		public var button_blocker:MovieClip;
		public var overview_top:MovieClip;
		public var overview_side_transition:MovieClip;
		public var overview_side:MovieClip;
		public var labels_mc:MovieClip;
		
		public function initSettings():void
		{
			display_mc.visible=false;
			display_mc.tab_mc.alpha=0;	
			bg_display_shape.alpha=0;
			button_blocker.visible=false;
			annealing_lehr_btn.alpha=0;
			tinbath_btn.alpha=0;
			furnace_btn.alpha=0;
			batch_house_btn.alpha=0;	
			overview_top.alpha=0;			
			processAnimation();
		}
		private function processAnimation():void
		{
			var timeline:TimelineMax = new TimelineMax({repeat:0});			
			timeline.append(TweenMax.to(overview_side_transition, .5, {delay:1, y:442, ease:Circ.easeOut}));			
			timeline.appendMultiple([TweenMax.to(overview_side_transition, .25, {alpha:0, scaleY:0}), TweenMax.to(overview_top, .25, {alpha:1, scaleY:1})]);
			timeline.append(TweenMax.to(overview_top, .05, {alpha:0, repeat:3, yoyo:true}));
			timeline.append(TweenMax.to(labels_mc, .05, {delay:.25, alpha:1, repeat:5, yoyo:false}));
		}
		
		
		//Initialize the FLV Player
		public function initFLVPlayer():void
		{			
			//---- Keeps the FLV Component from acting stupid ----//
			display_mc.myPlayer.fullScreenTakeOver=false;
			display_mc.myPlayer.addEventListener(VideoEvent.COMPLETE, onFLVComplete);
			
			function onFLVComplete(event:VideoEvent):void {
				 event.target.play();	  
			}
			
			//--------- Player Setup ---------//
			display_mc.myPlayer.source="gallery/location/World.f4v";
			display_mc.myPlayer.width = 1180;
			display_mc.myPlayer.height = 980;
			display_mc.myPlayer.volume = .2;
			display_mc.myPlayer.visible=false;			
			display_mc.myPlayer.autoRewind=true;
			display_mc.myPlayer.stop();
		}
		
		
		public function initButtons():void
		{			
			annealing_lehr_btn.addEventListener(MouseEvent.CLICK, sectorSelect);
			tinbath_btn.addEventListener(MouseEvent.CLICK, sectorSelect);
			furnace_btn.addEventListener(MouseEvent.CLICK, sectorSelect);
			batch_house_btn.addEventListener(MouseEvent.CLICK, sectorSelect);
			display_mc.addEventListener(MouseEvent.MOUSE_DOWN, closeDisplay);
		}
		
		private function sectorSelect(e:MouseEvent):void
		{
			button_blocker.visible=true;									
			
			if(e.currentTarget==annealing_lehr_btn)
			{				
				TweenMax.to(annealing_lehr_btn, .05, {alpha:1, repeat:3, yoyo:false, onComplete:selectSection01});								
				
				function selectSection01():void
				{									
					display_mc.myPlayer.source="gallery/annealing_lehr/Annealing_Lehr_3D.f4v";									
					display_mc.myPlayer.stop();
					displayAnimation();
				}				
			}
			else if (e.currentTarget==tinbath_btn)
			{				
				TweenMax.to(tinbath_btn, .05, {alpha:1, repeat:3, yoyo:false, onComplete:selectSection02});
				
				function selectSection02():void
				{
					display_mc.myPlayer.source="gallery/tin_bath/tin_bath_3D.f4v";				
					display_mc.myPlayer.stop();
					displayAnimation();
				}
			}
			else if (e.currentTarget==furnace_btn)
			{
				TweenMax.to(furnace_btn, .05, {alpha:1, repeat:3, yoyo:false, onComplete:selectSection03});
				
				function selectSection03():void
				{
					display_mc.myPlayer.source="gallery/furnace/furnace_3D.f4v";			
					display_mc.myPlayer.stop();
					displayAnimation();
				}				
			}
			else if (e.currentTarget==batch_house_btn)
			{
				TweenMax.to(batch_house_btn, .05, {alpha:1, repeat:3, yoyo:false, onComplete:selectSection04});
				
				function selectSection04():void
				{
					display_mc.myPlayer.source="gallery/batch_house/batch_house_3D.f4v";			
					display_mc.myPlayer.stop();
					displayAnimation();
				}						
			}			
			
			
		}

		public function closeDisplay(e:MouseEvent):void
		{					
			var bounds:Object = {left:1281, right:1920};
			var isDragging:Boolean = false;
			var offset:Number;			
				
			display_mc.addEventListener(MouseEvent.MOUSE_UP, onUp);				
			
			isDragging = true;
			offset = display_mc.mouseX;				
			addEventListener(MouseEvent.MOUSE_MOVE, onMove);								
			
			function onUp(e:MouseEvent):void
			{				
				if(display_mc.x>1400)
				{
					//var posOut = display_mc.x + 30;			
					display_mc.myPlayer.pause();
					
					var timeline:TimelineMax = new TimelineMax({repeat:0});
					timeline.append( TweenMax.to(display_mc, .5, {alphaAuto:0, x:2540, ease:Circ.easeInOut}));	
					timeline.append( TweenMax.to(bg_display_shape, .5, {scaleX:1.1, scaleY:1.1, alpha:0, blurFilter:{blurX:20, blurY:20}, onComplete:outComplete}));
					
					function outComplete():void
					{
						display_mc.visible=false;
						display_mc.myPlayer.pause();
						display_mc.myPlayer.visible=false;
						display_mc.tab_mc.visible=false;
						button_blocker.visible=false;
						annealing_lehr_btn.alpha=0;
						tinbath_btn.alpha=0;
						furnace_btn.alpha=0;
						batch_house_btn.alpha=0;
					}					
				}
				else
				{
					TweenMax.to(display_mc, .5, {x:1281});					
				}
				
				isDragging = false;
				removeEventListener(MouseEvent.MOUSE_MOVE, onMove);
			}
			
			function onMove(e:MouseEvent):void
			{
				display_mc.x = mouseX - offset;
				if(display_mc.x <= bounds.left)
					display_mc.x = bounds.left;
				else if(display_mc.x >= bounds.right)
					display_mc.x = bounds.right;
				e.updateAfterEvent();
				
				/*
				if(mouseY<560 || mouseY>598)
				{
					trace("out");
					TweenMax.to(display_mc, .5, {x:476});
					isDragging = false;
					removeEventListener(MouseEvent.MOUSE_MOVE, onMove);
				}
				*/
			}

		
		}		
		
		private function displayAnimation():void
		{		
			display_mc.x=1281;
			display_mc.visible=true;
			display_mc.tab_mc.visible=true;
			display_mc.tab_mc.alpha=0;
			display_mc.reveal_mc.visible=true;
			display_mc.reveal_mc.alpha=1;			
			
			
			var timeline:TimelineMax = new TimelineMax({repeat:0});
			timeline.append(TweenMax.to(bg_display_shape, .5, {delay:.5, scaleX:1, scaleY:1, alpha:.5, blurFilter:{blurX:0, blurY:0}}));
			timeline.append(TweenMax.from(display_mc.mask05, .5, {y:-992, ease:Quart.easeInOut}));	
			timeline.append(TweenMax.from(display_mc.frame_mc.mask01, .2, {scaleX:0}));
			timeline.append(TweenMax.from(display_mc.frame_mc.mask02, .2, {y:-990}));
			timeline.appendMultiple([TweenMax.from(display_mc.frame_mc.mask03, .1, {x:-886}), TweenMax.from(display_mc.frame_mc.mask04, .1, {x:886})]);
			timeline.append(TweenMax.from(display_mc.corners_mc, .5, {scaleX:1.1, scaleY:1.1, alpha:0}));
			timeline.append(TweenMax.to(display_mc.corners_mc, .05, {repeat:5, yoyo:true, alpha:0, onComplete:setCorners}));
			timeline.append(TweenMax.from(display_mc.lines_mc, .25, {width:1092, height:910, alpha:0, ease:Quint.easeOut, onComplete:playVideo}));							
			timeline.append(TweenMax.to(display_mc.reveal_mc, .5, {autoAlpha:0}));
			timeline.append(TweenMax.to(display_mc.tab_mc, .25, {alpha:1}));			
			
					
			function setCorners():void
			{
				display_mc.corners_mc.alpha = 1;
			}
			
			function playVideo():void
			{
				display_mc.myPlayer.autoRewind=true;
				display_mc.myPlayer.visible=true;
				display_mc.myPlayer.play();				
			}
		}
		
		
		public function initDisplay():void
		{	
			initSettings()
			initFLVPlayer();		
			initButtons();
		}
	
	
	
		public function OverviewPage()
		{
			super();
			alpha = 0;
			initDisplay();
			//new Scaffold(this);
		}
		override public function transitionIn():void 
		{
			super.transitionIn();
			TweenMax.to(this, 0.3, {alpha:1, onComplete:transitionInComplete});
		}
		override public function transitionOut():void 
		{
			super.transitionOut();
			TweenMax.to(this, 0.3, {alpha:0, onComplete:transitionOutComplete});
		}
	}
}
