﻿package pages
{
	import com.gaiaframework.templates.AbstractPage;
	import com.gaiaframework.events.*;
	import com.gaiaframework.debug.*;
	import com.gaiaframework.api.*;
	import fl.video.*;
	import flash.display.*;
	import flash.events.*;
	import com.greensock.*;
	import com.greensock.easing.*;
	import flash.geom.Point;
	
	public class UsaPage extends AbstractPage
	{	
	
		//MovieClip Declarations
		public var display_mc:MovieClip;		
		public var readout_mc:MovieClip;			
		
			
		//Initialize the FLV Player
		public function initFLVPlayer():void
		{			
			//---- Keeps the FLV Component from acting stupid ----//
			display_mc.myPlayer.fullScreenTakeOver=false;
			display_mc.myPlayer.addEventListener(VideoEvent.COMPLETE, onFLVComplete);
			
			function onFLVComplete(event:VideoEvent):void {
				 event.target.play();	  
			}
			
			//--------- Player Setup ---------//
			display_mc.myPlayer.source="gallery/location/US.f4v";
			display_mc.myPlayer.width = 1180;
			display_mc.myPlayer.height = 980;
			display_mc.myPlayer.volume = .2;
			display_mc.myPlayer.visible=false;			
			display_mc.myPlayer.stop();
		}
		
		public function initDisplay():void
		{	
			initFLVPlayer();		
			
			readout_mc.bg_mc.alpha = 0;
			readout_mc.bottom_pattern_mc.alpha = 0;
			readout_mc.base_mc.alpha = 0;
			readout_mc.title_mc.alpha = 0;
			readout_mc.shape01.alpha = 0;
			readout_mc.shape02.alpha = 0;
			readout_mc.shape03.alpha = 0;
			readout_mc.shape04.alpha = 0;
			readout_mc.shape05.alpha = 0;
			readout_mc.shape06.alpha = 0;
			readout_mc.shape07.alpha = 0;			
			readout_mc.key01.alpha = 0;
			readout_mc.key02.alpha = 0;
			readout_mc.key03.alpha = 0;
			readout_mc.key04.alpha = 0;
			readout_mc.key05.alpha = 0;
			readout_mc.key06.alpha = 0;
			
			var timeline:TimelineMax = new TimelineMax({repeat:0});
			timeline.append(TweenMax.from(display_mc.frame_mc.mask01, .2, {delay:.5, scaleX:0}));
			timeline.append(TweenMax.from(display_mc.frame_mc.mask02, .2, {y:-990}));
			timeline.appendMultiple([TweenMax.from(display_mc.frame_mc.mask03, .1, {x:-886}), TweenMax.from(display_mc.frame_mc.mask04, .1, {x:886})]);
			timeline.append(TweenMax.from(display_mc.corners_mc, .5, {scaleX:1.1, scaleY:1.1, alpha:0}));
			timeline.append(TweenMax.to(display_mc.corners_mc, .05, {repeat:5, yoyo:true, alpha:0, onComplete:setCorners}));
			timeline.append(TweenMax.from(display_mc.lines_mc, .25, {width:1092, height:910, alpha:0, ease:Quint.easeOut}));	
			timeline.append(TweenMax.from(display_mc.mask05, .5, {y:-992, ease:Quart.easeInOut, onComplete:playVideo}));				
			timeline.append(TweenMax.to(display_mc.reveal_mc, .5, {autoAlpha:0, onComplete:readoutAnimation}));	

		
			function setCorners():void
			{
				display_mc.corners_mc.alpha = 1;
			}
			
			function playVideo():void
			{
				display_mc.myPlayer.autoRewind=true;
				display_mc.myPlayer.visible=true;
				display_mc.myPlayer.play();				
			}
			
			function readoutAnimation():void
			{	
				var timeline:TimelineMax = new TimelineMax({repeat:0});
				timeline.append(TweenMax.to(readout_mc.bg_mc, .25, {autoAlpha:1}));
				timeline.append(TweenMax.fromTo(readout_mc.bottom_pattern_mc, .25, {y:62, alpha:0}, {y:87.6, alpha:1}), .4);	
				timeline.append(TweenLite.to(readout_mc.title_mc, 0.5, {x:-562.95, alpha:1, motionBlur:true, ease:Cubic.easeInOut}));
				timeline.append(TweenMax.to(readout_mc.base_mc, .25, {autoAlpha:1}));				
				
				timeline.appendMultiple( TweenMax.allFromTo([readout_mc.shape01,
														 readout_mc.shape02,
														 readout_mc.shape03,
														 readout_mc.shape04, 
														 readout_mc.shape05, 
														 readout_mc.shape06, 
														 readout_mc.shape07, 														
														 readout_mc.key01,
														 readout_mc.key02,
														 readout_mc.key03,
														 readout_mc.key04,
														 readout_mc.key05,
														 readout_mc.key06], .25, 
														 {autoAlpha:1, tint:0xFFFFFF}, {autoAlpha:1, tint:null}, 0.1));													 

			}
			
			
		}
	
		public function UsaPage()
		{
			super();
			alpha = 0;
			initDisplay();
			//new Scaffold(this);
		}
		override public function transitionIn():void 
		{
			super.transitionIn();
			TweenMax.to(this, 0.3, {alpha:1, onComplete:transitionInComplete});
		}
		override public function transitionOut():void 
		{
			super.transitionOut();
			TweenMax.to(this, 0.3, {alpha:0, onComplete:transitionOutComplete});
		}
	}
}
