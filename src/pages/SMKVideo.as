﻿package pages{  
 
	import flash.media.Video; 
	import flash.net.NetConnection; 
	import flash.net.NetStream; 
	import flash.events.NetStatusEvent; 
	import flash.events.AsyncErrorEvent;  
	//import flash.events.NetDataEvent;
	import flash.events.IOErrorEvent;
	import flash.events.Event;
 
	public class SMKVideo extends Video {  
		
		
		///////////////try reimporting all classes into fla folder instead of pages???
  
		
		private var URL:String;  
		private var _autoLoad:Boolean;		
		private var _autoPlay:Boolean;		
		private var _loop:Boolean;
		
	
		private var connection:NetConnection 
		private var stream:NetStream; 
		private var playedOnce:Boolean = false;  
	
		/** 
		 * 
		 * @param	relative or absolute path to .flv file 
		 */ 
		public function SMKVideo(videoPath:String, autoLoad:Boolean = true, autoPlay:Boolean = true, loop:Boolean = true):void{ 

			URL = videoPath;
			_autoLoad = autoLoad;
			_autoPlay = autoPlay; //TODO build out autoLoad feature? or use gestureworks system?
			_loop = loop;
 
			connection = new NetConnection(); 
            connection.addEventListener(NetStatusEvent.NET_STATUS, onStatus); 
            connection.connect(null);
			
		}  
 
		public function play():void { 
			stream.resume(); 
		}  
 
		public function pause():void { 
			stream.pause(); 
		}
		
		public function stop():void { 
			stream.seek(0); 
			stream.pause();
			
		}
		
		public function seek(time:int):void { 
			stream.seek(time); 
			
		}
		
		public function unloadVideo():void { 
			
			//unloadVideo(); //this commands generates stack overflow!
			//made my own!
			attachNetStream(null); 
			stream.close();
			connection.close();
			clear();
			
		}
		
		public function getNetStream():NetStream{
			
			return stream;
			
		}

		public function getNetConnection():NetConnection{
			
			return connection;
			
		}
		
		
		/*
		private function onComplete(nde:NetDataEvent):void{ 
			trace("onComplete()", nde.info.handler, this.name);
			
			dispatchEvent(new Event(Event.COMPLETE));
			//if (nse.info.code == "NetStream.Play.Stop" && playedOnce && _loop) { //not seamless
			//if (nse.info.code == "NetStream.Buffer.Empty" && playedOnce && _loop) { //seamless
			if (nde.info.handler == "onPlayStatus" && playedOnce && _loop) { //seamless uses real event instead of buffer
			
				stream.seek(0);
				
			} 
		}
		*/

		private function onStatus(nse:NetStatusEvent):void { 
			if (nse.info.code == "NetConnection.Connect.Success" && !playedOnce){ 
				playedOnce = true;
				initVideo();
			}
			
			
			//if (nse.info.code == "NetStream.Play.Stop" && playedOnce && _loop) { //not seamless
			//if (nse.info.code == "NetStream.Buffer.Empty" && playedOnce && _loop) { //seamless
			if(nse.info.code == "NetStream.Play.Stop" && playedOnce && _loop) {//seamless uses real event instead of buffer
			
				stream.seek(0);
				
			}  
		}  
		
		
		private function asyncErrorHandler(event:AsyncErrorEvent):void { 
			//trace("AsyncErrorEvent",event.error, event.errorID, event.text);
			//dispatchEvent(event); 
			
			//build out
			//http://www.adobe.com/devnet/flash/quickstart/metadata_cue_points.html
			
        }  
		
		
		private function ioErrorHandler(event:IOErrorEvent):void { 
			trace("ioErrorHandler",event.errorID, event.text);
			//dispatchEvent(event);
			
        }  
		
		
 
		public function initVideo():void{
 
			stream = new NetStream(connection); 
			stream.addEventListener(NetStatusEvent.NET_STATUS, onStatus); 
			//stream.addEventListener(NetDataEvent.MEDIA_TYPE_DATA, onComplete); 
			stream.addEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler); 
			stream.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			
            
			attachNetStream(stream);
			
            stream.play(URL);
			
			
			if(!_autoPlay){
				stream.pause();
			}

		}
	} 
}