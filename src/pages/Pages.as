﻿/*****************************************************************************************************
* Gaia Framework for Adobe Flash ©2007-2009
* Author: Steven Sacks
*
* blog: http://www.stevensacks.net/
* forum: http://www.gaiaflashframework.com/forum/
* wiki: http://www.gaiaflashframework.com/wiki/
* 
* By using the Gaia Framework, you agree to keep the above contact information in the source code.
* 
* Gaia Framework for Adobe Flash is released under the MIT License:
* http://www.opensource.org/licenses/mit-license 
*****************************************************************************************************/

package pages
{	
	public class Pages
	{
		public static const INDEX:String = "index";
		public static const NAV:String = "index/nav";
		public static const HOME:String = "index/nav/home";
		public static const LOCATION:String = "index/nav/location";
		public static const WORLD:String = "index/nav/location/world";
		public static const USA:String = "index/nav/location/usa";
		public static const PORTAGE:String = "index/nav/location/portage";
		public static const PROCESS:String = "index/nav/process";
		public static const OVERVIEW:String = "index/nav/process/overview";
		public static const BATCH_HOUSE:String = "index/nav/process/batch_house";
		public static const FURNACE:String = "index/nav/process/furnace";
		public static const TIN_BATH:String = "index/nav/process/tin_bath";
		public static const ANNEALING_LEHR:String = "index/nav/process/annealing_lehr";
		public static const GALLERY:String = "index/nav/gallery";
		public static const SPR_2009:String = "index/nav/gallery/SPR_2009";
		public static const FWR_2010:String = "index/nav/gallery/FWR_2010";
		public static const FR_2012:String = "index/nav/gallery/FR_2012";
		public static const MISC:String = "index/nav/gallery/Misc";
	}
}
