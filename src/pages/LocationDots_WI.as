﻿package pages{
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	
	public class LocationDots_WI extends MovieClip {
		
		private var _parentScope:MovieClip;
		private var _dots:Array = new Array();
		
		public function LocationDots_WI(scope:MovieClip) {
			// constructor code

			_parentScope = scope;
			
			initDots();

			//*****************************************************************
			_dots[0] = ["AMERY LG","Amery, Wisconsin"];
			
			_dots[1] = ["TOMAH IG","Tomah, Wisconsin"];
			_dots[2] = ["TOMAH TG","Tomah, Wisconsin"];
			
			_dots[3] = ["MAZOMANIE CG", "Mazomanie, Wisconsin"];
			

			_dots[4] = ["SPRING GREEN AG", "Spring Green, Wisconsin"];
			_dots[5] = ["SPRING GREEN CG", "Spring Green, Wisconsin"];
			_dots[6] = ["SPRING GREEN IG", "Spring Green, Wisconsin"];
			
			_dots[7] = ["PORTAGE FG", "Portage, Wisconsin"];
			
			_dots[8] = ["MENOMONIE FG", "Menomonie, Wisconsin"];
			//*****************************************************************
			
		}
		
		private function initDots():void{
			for(var i:uint = 0; i < this.numChildren; i++){
				this["DOT_"+i].addEventListener(MouseEvent.CLICK, onDotClicked);
			}
			
		}
		
		public function getDotData(dotIndex:uint):Array{
			
			return _dots[dotIndex];
			
		}
		
		private function onDotClicked(event:MouseEvent):void{
			
			_parentScope.loadLocationData(this.name.split("_")[1], event.target.name);
		}
	
		
	}
}
