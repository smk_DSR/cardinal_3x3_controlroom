﻿package pages
{
	import com.gaiaframework.templates.AbstractPage;
	import com.gaiaframework.events.*;
	import com.gaiaframework.debug.*;
	import com.gaiaframework.api.*;
	import fl.video.*;
	import flash.display.*;
	import flash.events.*;
	import com.greensock.*;
	import com.greensock.easing.*;
	import flash.filters.BlurFilter;
	
	public class PortagePage extends AbstractPage
	{	
	
		//MovieClip Declarations
		public var display_mc:MovieClip;	
		public var readout_mc:MovieClip;
		
		public var topLayer:MovieClip;
		private var _smu:StateMapsUI;
		private var homeNavBack:MovieClip;
		private var touchMe:MovieClip;

		
		public function initDisplay():void
		{	
			trace("initDisplay()");
			
			var timeline:TimelineMax = new TimelineMax({repeat:0});
			trace("initDisplay()2");
			timeline.append(TweenMax.from(display_mc.frame_mc.mask01, .2, {delay:.5, scaleX:0}));
			trace("initDisplay()3");
			timeline.append(TweenMax.from(display_mc.frame_mc.mask02, .2, {y:-990}));
			trace("initDisplay()4");
			timeline.appendMultiple([TweenMax.from(display_mc.frame_mc.mask03, .1, {x:-886}), TweenMax.from(display_mc.frame_mc.mask04, .1, {x:886})]);
			trace("initDisplay()5");
			timeline.append(TweenMax.from(display_mc.corners_mc, .5, {scaleX:1.1, scaleY:1.1, alpha:0}));
			trace("initDisplay()6");
			timeline.append(TweenMax.to(display_mc.corners_mc, .05, {repeat:5, yoyo:true, alpha:0, onComplete:setCorners}));
			trace("initDisplay()7");
			timeline.append(TweenMax.from(display_mc.lines_mc, .25, {width:1092, height:910, alpha:0, ease:Quint.easeOut}));	
			trace("initDisplay()8");
			timeline.append(TweenMax.from(display_mc.mask05, .5, {y:-992, ease:Quart.easeInOut}));				
			trace("initDisplay()9");
			timeline.append(TweenMax.to(display_mc.reveal_mc, .5, {autoAlpha:0, onComplete:activateMap}));	

			trace("initDisplay()10");
		
			function setCorners():void
			{
				trace("setCorners()");
				
				display_mc.corners_mc.alpha = 1;

			}
			
			
		}
		
		public function activateMap():void{
			trace("activateMap()");
			
			hideNavItems();
			
			topLayer = new MovieClip();
			topLayer.graphics.beginFill(0x0000FF, 1);
			topLayer.graphics.drawRect(0,0,1920,1080);
			topLayer.graphics.endFill();
			topLayer.x = 0;
			topLayer.y = 0;
			topLayer.width = 1920;
			topLayer.height = 1080;
			addChild(topLayer);
			
			touchMe = new HomeTouchToBeganTxt();
			touchMe.x = 1509;
			touchMe.y = 532;
			topLayer.addChild(touchMe);

			trace("_smu1");
			_smu = new StateMapsUI(topLayer);
			//addChild(_smu);
			//TweenLite.to(this, 13, {onComplete:function(){addBackHomeNav();}});
			
		}
	
		public function PortagePage()
		{
			super();
			alpha = 0;
			initDisplay();
			//new Scaffold(this);
			
			
		}

		private function hideNavItems():void{
			trace("hideNavItems()");
			
			//FADE OUT
			var content:MovieClip = Gaia.api.getPage("index/nav").content;
			
			content.SUB_Location_mc.visible = false;
			content.nav_display_mc.visible = false;
			content.corner_waves.visible = false;
			content.title_mc.visible = false;
			content.main_nav_mc.visible = false;
			content.read_out_mc.visible = false;
			content.bottom_bar_mc.visible = false;
			content.frames_mc.visible = false;	
		}
		
		override public function transitionIn():void 
		{
			super.transitionIn();
			TweenMax.to(this, 0.3, {alpha:1, onComplete:transitionInComplete});
		}
		override public function transitionOut():void 
		{
			super.transitionOut();
			TweenMax.to(this, 0.3, {alpha:0, onComplete:transitionOutComplete});
		}
		
		function onHomeNavTouch(e:*):void{
			trace("onHomeNavTouch", e.target.name);
				
			_smu.unloadVideos();
			_smu.closeLocationDetailsWindow();
			topLayer.removeChild(_smu);
			topLayer.removeChild(topLayer.getChildByName("MapArrows"));
			
			
			exitHomeNavBack();
			
			touchMe.play();
		}
		
		function exitHomeNavBack():void{
			TweenLite.to(homeNavBack, .5, {x:960, y:540, z:10000, scaleX:0.1, scaleY:0.1, scaleZ:0.1, ease:Strong.easeInOut, onComplete:exitHomeNavBackComplete});
		}
		
		function homeNavBackStart():void{
	
			topLayer.addChild(homeNavBack);
			homeNavBack.visible = true;
		
		}
		
		function homeNavBackComplete():void{
			homeNavBack.filters = null; 
	
			
		}


		function exitHomeNavBackComplete():void{
			homeNavBack.filters = null;
			topLayer.removeChild(homeNavBack);
			touchMe.play();
		}



		function addBackHomeNav():void{
			trace("addBackHomeNav()");

			homeNavBack = new HomeNavBack();
			homeNavBack.x = 59;
			homeNavBack.y = 88;

			homeNavBack.addEventListener(MouseEvent.CLICK, onHomeNavTouch);
			homeNavBack.addEventListener(TouchEvent.TOUCH_TAP, onHomeNavTouch);

			
			homeNavBack.visible = false;
			homeNavBack.x = 480;
			homeNavBack.y = 200;
			homeNavBack.z = -1000;
			homeNavBack.scaleX = 20;
			homeNavBack.scaleY = 20;
			homeNavBack.scaleZ = 20;
			homeNavBack.cacheAsBitmap = true;
			var f:Array = homeNavBack.filters;
			f[0] = new BlurFilter(4, 8, 1);
			homeNavBack.filters = f;
			
			TweenLite.to(homeNavBack, .33, {delay:.5, x:59, y:88, z:0, scaleX:1, scaleY:1, scaleZ:1, ease:Quad.easeInOut, onComplete:homeNavBackComplete, onStart:homeNavBackStart});
	
}
	}
}
