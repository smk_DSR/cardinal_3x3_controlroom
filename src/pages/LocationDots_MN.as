﻿package pages{
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	
	public class LocationDots_MN extends MovieClip {
		
		private var _parentScope:MovieClip;
		private var _dots:Array = new Array();
		
		public function LocationDots_MN(scope:MovieClip) {
			// constructor code

			_parentScope = scope;
			
			initDots();

			//*****************************************************************
			_dots[0] = ["CARDINAL CORPORATE OFFICE","Eden Prairie, Minnesota"];
			_dots[1] = ["NORTHFIELD CG","Northfield, Minnesota"];
			//*****************************************************************
			
		}
		
		private function initDots():void{
			for(var i:uint = 0; i < this.numChildren; i++){
				this["DOT_"+i].addEventListener(MouseEvent.CLICK, onDotClicked);
			}
			
		}
		
		public function getDotData(dotIndex:uint):Array{
			
			return _dots[dotIndex];
			
		}
		
		private function onDotClicked(event:MouseEvent):void{
			
			_parentScope.loadLocationData(this.name.split("_")[1], event.target.name);
		}
	
		
	}
}
