﻿package pages{
	
	import flash.display.MovieClip;
	import flash.media.Video;
	import flash.net.NetStream;
	import flash.events.NetStatusEvent;
	import flash.net.NetStreamInfo;
	import flash.utils.getDefinitionByName;
	import flash.geom.Point;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.SimpleButton;
	import flash.geom.Rectangle;
	//import com.gestureworks.utils.ArrangePoints;
	import flash.display.DisplayObjectContainer;
	import flash.display.FrameLabel;
	import com.greensock.TweenLite;
	import flash.events.Event;
	import com.greensock.easing.Strong;

	
	
	public class StateMapsUI extends MovieClip{
		
		public const HOME_STATE:String = "WI";
		public const HOME_CITY_DOT:String = "DOT_7";
		
		public var _currentState:String = "";
		public var _currentCityDot:String = "";
		
		private var _parentScope:MovieClip;
		
		public var PlantImages:MovieClip;
		//private var _globeVideo:SMKVideo;
		
		private var _usaVideo:SMKVideo;
		
		public var videoHolder:MovieClip;
		public var usaMapNav:MovieClip;
		
		
		
		private var _stateVideo:SMKVideo;
		
		//private var _gvNS:NetStream;
		private var _usavNS:NetStream;
		private var _statesvNS:NetStream;		
		
		private var _cfgStats:CFGStats;
		private var _locationDetailsWindowMC:LocationDetailsWindow;
		private var _ldwX:Number = 978;
		private var _ldwY:Number = 182;
		
		//private var _targetHUD:TargetHud;
		
		//States Dataset
		private var _stateVids:Array = new Array();
		private var _locationDots:Array = new Array();
		
		private var _locationDotsMC:MovieClip;
		private var _dotsText:Array = new Array();
		
		private var _umt:USAMapTransition;
		
		
		
		public function StateMapsUI(scope:MovieClip){
			trace("StateMapsUI()");
			 
			_parentScope = scope;
			trace("1");
			loadVideos();
			trace("2");
			//TODO refactor
			//_gvNS = _globeVideo.getNetStream();
			//_gvNS.addEventListener(NetStatusEvent.NET_STATUS, globevStatus);
			
			//_usavNS = _usaVideo.getNetStream();
			//_usavNS.addEventListener(NetStatusEvent.NET_STATUS, usavStatus);
			
			//_statesvNS = _usaVideo.getNetStream();			
			//_statesvNS.addEventListener(NetStatusEvent.NET_STATUS, statevStatus);
			
			//initData();
			
		}
	
		
		private function loadVideos():void{
			trace("loadVideos()");
			

			//_globeVideo = new SMKVideo("library/assets/videos/Globe_02.mp4", true, false, false);
			//_globeVideo.x = 0;
			//_globeVideo.y = 0;
			//_globeVideo.width = 1026;//1180
			//_globeVideo.height = 852;//980
			//
			
			trace("xxx1");

			_usaVideo = new SMKVideo("library/assets/videos/USA_02.mp4", true, false, false);
			//_usaVideo = new SMKVideo();
			trace("xxx2");			
			//_usaVideo.x = 0;
			//_usaVideo.y = 0;
			//_usaVideo.width = 1026;//1180
			//_usaVideo.height = 852;//980
			//
			
			//videoHolder.addChild(_usaVideo);
			//_globeVideo.seek(5);
			//_usaVideo.play();
			
			

		}
		
		private function loadStateVideo(state:String):void{
			trace("loadStateVideo()");
			
			var stateVidPath:String = _stateVids[state];
			
			if(_stateVideo){
				_stateVideo.unloadVideo();
			}
			
			//TODO load on demand states
			_stateVideo = new SMKVideo(stateVidPath, true, true, true);
			//_stateVideo = new SMKVideo();
			_stateVideo.x = 0;
			_stateVideo.y = 0;
			_stateVideo.width = 1026;
			_stateVideo.height = 852;
			videoHolder.addChild(_stateVideo);
			_stateVideo.play();

		}
		
		
		//private	function globevStatus(e:NetStatusEvent):void{
		//	
		//	if(e.info.code == "NetStream.Play.Stop"){
		//			
		//		videoHolder.addChild(_usaVideo);
		//		//_usaVideo.seek(3);
		//		_usaVideo.play();
		//		
		//		_cfgStats = new CFGStats();
		//		_cfgStats.x = 1566.85;
		//		_cfgStats.y = 73.45;
		//		addChild(_cfgStats);
		//		_globeVideo.unloadVideo();
		//		videoHolder.removeChild(_globeVideo);
		//		
		//		
		//	} 
		//	
		//}


		private function usavStatus(e:NetStatusEvent):void{
			trace("usavStatus()",e.info.code);
			
			if(e.info.code == "NetStream.Play.Stop"){
				trace("~~~YES");
				
				_umt = new USAMapTransition();
				_umt.alpha = 1;
				_umt.x = 730;
				_umt.y = 240;
				_umt.scaleX = 1.61;
				_umt.scaleY = 1.61;
				
				trace("^^^^^^***",_parentScope.name);
				_parentScope.addChild(_umt);
				TweenLite.to(_umt, .5, {x:-140, y:400, scaleX:1, scaleY:1, ease:Strong.easeIn, onComplete:umtComplete});
				//TweenLite.to(umt, .5, {scaleX:100, y:500});
				
				videoHolder.removeChild(_usaVideo);
				_usaVideo.unloadVideo();
				
			} 
	
		}

		private function statevStatus(e:NetStatusEvent):void{
			
			if(e.info.code == "NetStream.Play.Stop"){
				
				//TODO???
			} 
			
		}
		
		private function loadStateData(state:String, cityDot:String):void{
			trace("???loadStateData()",state,cityDot);
			
			_currentState = state;
			_currentCityDot = cityDot;
			
			//TODO unload state video?
			loadStateVideo(state);
			
			
			if(_locationDotsMC){
				removeChild(_locationDotsMC);
				_locationDotsMC = null;
			}
			
			_locationDotsMC = new _locationDots[state][0](this);
			_locationDotsMC.x = _locationDots[state][1];//908.5;
			_locationDotsMC.y = _locationDots[state][2];//397.30;
			addChild(_locationDotsMC);
			
			
			if(state == HOME_STATE && cityDot == ""){//set to Portage
				//setTargetHUD(new Point(1338, 805));
			}else{
				//setTargetHUD(new Point(1338, 150));//TODO city Point
			}
			
			loadLocationData(state, cityDot);
			
			
		}
		
		public function loadLocationData(state:String, cityDot:String):void{
			trace("???loadLocationData",state,cityDot);
			
			if(state != ""){
				_currentState = state;
			}
			
			if(cityDot != ""){
				_currentCityDot = cityDot;
			}
			
			
			
			try{
				if(_locationDetailsWindowMC){
					removeChild(_locationDetailsWindowMC);	
					_locationDetailsWindowMC = null;
				}
			}catch(e:Error){
				//ignore
			}

			
			if(state == HOME_STATE && cityDot == ""){
				cityDot = HOME_CITY_DOT;
				_currentCityDot = cityDot;
			}
			

			trace("^^^cityDot",cityDot);
			
			var cityDotNumber:int;
			
			if(cityDot != ""){
				
				cityDotNumber = cityDot.split("_")[1];
				
				if(!_locationDetailsWindowMC){
					_locationDetailsWindowMC = new LocationDetailsWindow(this);
					_locationDetailsWindowMC.x = _ldwX;
					_locationDetailsWindowMC.y = _ldwY;
					_locationDetailsWindowMC.addEventListener(MouseEvent.MOUSE_DOWN, onLDWStartDrag);
					_locationDetailsWindowMC.addEventListener(MouseEvent.MOUSE_UP, onLDWStopDrag);
				}
			
					
				
				var plantTypeIndex:int = _locationDotsMC.getDotData(cityDotNumber)[0].split(" ").length-1;
				var plantType:String = _locationDotsMC.getDotData(cityDotNumber)[0].split(" ")[plantTypeIndex];
				
				
				//_locationDetailsWindowMC.PlantImagexs.gotoAndStop(Math.ceil((Math.random()*29)));
				if(movieClipHasLabel(_locationDetailsWindowMC.PlantImages, _locationDotsMC.getDotData(cityDotNumber)[0])){
					
					_locationDetailsWindowMC.PlantImages.gotoAndStop(_locationDotsMC.getDotData(cityDotNumber)[0]);
					
				}else{
					
					_locationDetailsWindowMC.PlantImages.gotoAndStop("MissingLabel");
				}

				
				
				//exceptions//
				if(plantType.toLowerCase() == "minerals"){
					plantType = "DURANT_MINERALS";
				}
				
				if(plantType.toLowerCase() == "office"){
					plantType = "office";
				}
				
				if(plantType == "AG"){
					plantType = "AG_RD_HQ";//yes i know... hack hack hack
				}
				trace("plantType ",plantType);
				
				if(_locationDotsMC.getDotData(cityDotNumber)[0] == "CARDINAL CORPORATE OFFICE"){
					_locationDetailsWindowMC.setHeader(_locationDotsMC.getDotData(cityDotNumber)[0], 35);
				}else{
					_locationDetailsWindowMC.setHeader(_locationDotsMC.getDotData(cityDotNumber)[0], 39);//font size 39
				}
				//exceptions//
				switch(_locationDotsMC.getDotData(cityDotNumber)[0]){
					
					case "HOOD RIVER IG":
					case "FARGO IG":
					case "WAXAHACHIE IG":
					case "GREENFIELD IG":
					case "AMERY LG":
					case "TOMAH IG":
					case "SPRING GREEN IG":
					case "FREMONT IG":
					case "OCALA LG":
					case "MOUNTAIN TOP IG":
					case "ROANOKE IG":
					
					_locationDetailsWindowMC.setBody(_locationDotsMC.getDotData(cityDotNumber)[1]+"\n"+_dotsText[plantType], 14);//smaller size 14
					break;
					
					default://normal size 16
						_locationDetailsWindowMC.setBody(_locationDotsMC.getDotData(cityDotNumber)[1]+"\n"+_dotsText[plantType], 16);
					break;
				}
				
				
				_locationDetailsWindowMC.checkFeatures();
				
				addChild(_locationDetailsWindowMC);
				
			}
			
		}
		
		
		public function closeLocationDetailsWindow():void{
			
			
			removeChild(_locationDetailsWindowMC);
			_locationDetailsWindowMC = null;
			
			
		}
		
		private function setTargetHUD(p:Point):void{
			
			/*if(!_targetHUD){
				_targetHUD = new TargetHud();
			}
			
			_targetHUD.x = p.x;
			_targetHUD.y = p.y;
			addChild(_targetHUD);
		*/	
		}
		
		private function umtComplete():void{
			trace("umtComplete()");
			
			TweenLite.to(_umt, .5, {alpha:0, onComplete:function(){_parentScope.removeChild(_umt);}});
			loadStateData(HOME_STATE, "");
			initUSAMapNav();
			
		}
		
		private function initUSAMapNav():void{
			
			
			MovieClip(usaMapNav.usaMap_MC).addEventListener(MouseEvent.MOUSE_DOWN, usaMapStartDrag);
			MovieClip(usaMapNav.usaMap_MC).addEventListener(MouseEvent.MOUSE_UP, usaMapStopDrag);
			//MovieClip(usaMapNav.usaMap_MC).addEventListener(MouseEvent.RELEASE_OUTSIDE, usaMapStopDrag);
			
			
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.AZ_Btn).addEventListener(MouseEvent.CLICK, onStateClick);
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.AZ_Btn).useHandCursor = false;
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.CA_Btn).addEventListener(MouseEvent.CLICK, onStateClick);
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.CA_Btn).useHandCursor = false;
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.CO_Btn).addEventListener(MouseEvent.CLICK, onStateClick);
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.CO_Btn).useHandCursor = false;
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.FL_Btn).addEventListener(MouseEvent.CLICK, onStateClick);
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.FL_Btn).useHandCursor = false;
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.GA_Btn).addEventListener(MouseEvent.CLICK, onStateClick);
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.GA_Btn).useHandCursor = false;
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.IA_Btn).addEventListener(MouseEvent.CLICK, onStateClick);
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.IA_Btn).useHandCursor = false;
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.IN_Btn).addEventListener(MouseEvent.CLICK, onStateClick);
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.IN_Btn).useHandCursor = false;
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.MN_Btn).addEventListener(MouseEvent.CLICK, onStateClick);
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.MN_Btn).useHandCursor = false;
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.NC_Btn).addEventListener(MouseEvent.CLICK, onStateClick);
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.NC_Btn).useHandCursor = false;
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.ND_Btn).addEventListener(MouseEvent.CLICK, onStateClick);
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.ND_Btn).useHandCursor = false;
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.OK_Btn).addEventListener(MouseEvent.CLICK, onStateClick);
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.OK_Btn).useHandCursor = false;
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.OR_Btn).addEventListener(MouseEvent.CLICK, onStateClick);
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.OR_Btn).useHandCursor = false;
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.PA_Btn).addEventListener(MouseEvent.CLICK, onStateClick);
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.PA_Btn).useHandCursor = false;
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.TX_Btn).addEventListener(MouseEvent.CLICK, onStateClick);
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.TX_Btn).useHandCursor = false;
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.VA_Btn).addEventListener(MouseEvent.CLICK, onStateClick);
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.VA_Btn).useHandCursor = false;
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.WA_Btn).addEventListener(MouseEvent.CLICK, onStateClick);
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.WA_Btn).useHandCursor = false;
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.WI_Btn).addEventListener(MouseEvent.CLICK, onStateClick);
			SimpleButton(usaMapNav.usaMap_MC.USA_Btns.WI_Btn).useHandCursor = false;
			
			var ma:MapArrows = new MapArrows();
			ma.name = "MapArrows";
			ma.x = 254;
			ma.y = 554;
			_parentScope.addChild(ma);
			//this.setChildIndex(ma, this.numChildren);
			ma.gotoAndPlay(1);
			
			
		}

		private function onStateClick(e:MouseEvent):void{
			trace("onStateClick");
			
			var stateName:String = e.target.name.split("_")[0];
			loadStateData(stateName, "");
			
		}
		
		
		private function usaMapStartDrag(event:MouseEvent):void{
			MovieClip(event.target).startDrag(false, new Rectangle(-110,-124,332,165));
			
				
		}
		
		private function usaMapStopDrag(event:MouseEvent):void{
			MovieClip(event.target).stopDrag();
			
				
		}
		
		
		private function initData():void{
		
			
			
			
			_dotsText["IG"] = "Insulating glass (IG) plants take specific pieces of glass required and separate them by sealing a spacer in between them and filling the area between them with air or gas to then be placed into a frame of a door or window. Cardinal IG units deliver outstanding thermal performance and extremely low failure rates.";
			
			_dotsText["CG"] = "Coated Glass (CG) plants add layers of coatings to the glass which transmits visible light and reflects solar heat and far infrared. Cardinal employs patented, state-of-the-art sputter coating processes that are unmatched by any other glass manufacturer.";
			
			_dotsText["LG"] = "Laminated Glass (LG) plants take glass and add a layer(s) of polyvinyl butyral between two or more pieces of glass. Cardinal laminated glass consists of annealed, heat-strengthened or tempered glass with one or more transparent interlayers sandwiched together to create a stronger, sturdier glass unit.";
			
			_dotsText["FG"] = "Float Glass (FG) plants use a technique of pouring molten glass over a bath of molten tin in order to get a high quality flat glass product. Float Glass is the foundation of all Cardinal products.";
			
			_dotsText["TG"] = "Tempered Glass (TG) processing.";
			
			_dotsText["AG_RD_HQ"] = "Automation Group (AG) designs and builds automated machines to be installed in Cardinal facilities.";
			
			_dotsText["office"] = "Corporate Headquarters building.";
			
			_dotsText["DURANT_MINERALS"] = "Cardinal Glass Industry division for raw material mining and processing of sand.";
			
			
		
			_locationDots["WI"] = new Array(LocationDots_WI, 908, 397);
			_stateVids["WI"] = "library/assets/videos/WI map.mp4";
			
			_locationDots["FL"] = new Array(LocationDots_FL, 1450, 500);
			_stateVids["FL"] = "library/assets/videos/FL map.mp4";
		
			_locationDots["WA"] = new Array(LocationDots_WA, 908, 600);
			_stateVids["WA"] = "library/assets/videos/WA map.mp4";
			
			_locationDots["OK"] = new Array(LocationDots_OK, 1400, 600);
			_stateVids["OK"] = "library/assets/videos/OK map.mp4";
			
		
			_locationDots["VA"] = new Array(LocationDots_VA, 1200, 575);
			_stateVids["VA"] = "library/assets/videos/VA map.mp4";
		
			_locationDots["TX"] = new Array(LocationDots_TX, 1325, 650);
			_stateVids["TX"] = "library/assets/videos/TX map.mp4";
			
			_locationDots["PA"] = new Array(LocationDots_PA, 1550, 450);
			_stateVids["PA"] = "library/assets/videos/PA map.mp4";
			
			_locationDots["NC"] = new Array(LocationDots_NC, 1225, 525);
			_stateVids["NC"] = "library/assets/videos/NC map.mp4";
			
			_locationDots["OR"] = new Array(LocationDots_OR, 1200, 350);
			_stateVids["OR"] = "library/assets/videos/OR map.mp4";
			
			_locationDots["IA"] = new Array(LocationDots_IA, 1050, 600);
			_stateVids["IA"] = "library/assets/videos/IA map.mp4";
			
			
			_locationDots["IN"] = new Array(LocationDots_IN, 1375, 175);
			_stateVids["IN"] = "library/assets/videos/IN map.mp4";
			
			_locationDots["ND"] = new Array(LocationDots_ND, 1600, 475);
			_stateVids["ND"] = "library/assets/videos/ND map.mp4";
			
			
			_locationDots["GA"] = new Array(LocationDots_GA, 1108, 200);
			_stateVids["GA"] = "library/assets/videos/GA map.mp4";
			
			_locationDots["MN"] = new Array(LocationDots_MN, 1325, 780);
			_stateVids["MN"] = "library/assets/videos/MN map.mp4";

			_locationDots["CO"] = new Array(LocationDots_CO, 1400, 300);
			_stateVids["CO"] = "library/assets/videos/CO map.mp4";
			
			_locationDots["CA"] = new Array(LocationDots_CA, 1300, 850);
			_stateVids["CA"] = "library/assets/videos/CA map.mp4";
			
			_locationDots["AZ"] = new Array(LocationDots_AZ, 1300, 800);
			_stateVids["AZ"] = "library/assets/videos/AZ map.mp4";
			
		}
		
		
		private function getPath(displayObject:DisplayObject):String{
            var path:String = "";

            var name:String = (displayObject != stage) 
                ? displayObject.name : "stage";

            path += name + ".";

            if (displayObject.parent)
            {
                path = getPath(displayObject.parent) + path;

            }

            return path;

        }// end function
		
		
		public function unloadVideos():void{
			trace("~~~StateMapsUI.unloadVideos()");
			
			try{
				//_globeVideo.unloadVideo();
				//_usaVideo.unloadVideo();
				_stateVideo.unloadVideo();
			}catch(e:Error){
				//Ignore
			}
			
		}
		
		
	private function movieClipHasLabel(movieClip:MovieClip, labelName:String):Boolean {
	   var i:int;
	   var k:int = movieClip.currentLabels.length;
	   for (i; i < k; ++i) {
		  var label:FrameLabel = movieClip.currentLabels[i];
		  if (label.name == labelName)
			 return true;
	   }
	   return false;
	}
	
	private function onLDWStartDrag(event:MouseEvent):void{
	
		_locationDetailsWindowMC.startDrag(false);
		
	}
	
	private function onLDWStopDrag(event:MouseEvent):void{
	
		_locationDetailsWindowMC.stopDrag();
		_ldwX = _locationDetailsWindowMC.x;
		_ldwY = _locationDetailsWindowMC.y;
		
	}
		
	}//class
}
