﻿package pages
{
	import com.gaiaframework.templates.AbstractPage;
	import com.gaiaframework.events.*;
	import com.gaiaframework.debug.*;
	import com.gaiaframework.api.*;
	import flash.display.*;
	import flash.events.*;
	
	//Multitouch.inputMode = MultitouchInputMode.GESTURE;	
	import com.greensock.*;
	import com.greensock.easing.*;	
	
	public class NavPage extends AbstractPage
	{	
		
		//MovieClip Declarations
		public var SUB_Location_mc:MovieClip;
		public var SUB_Process_mc:MovieClip;
		public var SUB_Gallery_mc:MovieClip;
		
		public var frames_mc:MovieClip;
		public var nav_display_mc:MovieClip;
		public var read_out_mc:MovieClip;
		public var bottom_bar_mc:MovieClip;
		public var title_mc:MovieClip;
		public var main_nav_mc:MovieClip;	
		public var corner_waves:MovieClip;
		public var subNavSelection:MovieClip;
		public var seqNav:MovieClip;
		
		//delete this when done testing
		public var thumb:MovieClip;
		
		//public var offset:Number;	
		//thumb.buttonMode = true;
		
		public var mySubNav:MovieClip = new MovieClip();
		
	
	
		//Button Declarations
		private var buttons:Array;		
		public var BTN_Home:MovieClip;		
		
		public var BTN_Location:MovieClip;
		public var BTN_World:MovieClip;
		public var BTN_USA:MovieClip;
		public var BTN_Portage:MovieClip;
		
		public var BTN_Process:MovieClip;
		public var BTN_Overview:MovieClip;
		public var BTN_Batch_House:MovieClip;
		public var BTN_Furnace:MovieClip;
		public var BTN_Tin_Bath:MovieClip;
		public var BTN_Annealing_Lehr:MovieClip;
		
		public var BTN_Gallery:MovieClip;
		public var BTN_SPR_2009:MovieClip;
		public var BTN_FWR_2010:MovieClip;
		public var BTN_FR_2012:MovieClip;
		public var BTN_Misc:MovieClip;
		
				
		//Initialize Default Settings
		public function initSettings():void
		{
			SUB_Location_mc.visible=false;
			SUB_Process_mc.visible=false;
			SUB_Gallery_mc.visible=false;		
			nav_display_mc.seq_location.visible=false;
			nav_display_mc.seq_process.visible=false;
			nav_display_mc.seq_gallery.visible=false;
			
			
		}
		
		private function navAnimation():void
		{						
			subNavSelection.visible=true;
			var timeline01:TimelineMax = new TimelineMax({repeat:0});
			timeline01.append( TweenMax.to(main_nav_mc, .5, {autoAlpha:0}) );
			timeline01.append( TweenMax.to(corner_waves, .5, {delay:1.5, scaleX:0, scaleY:0}) );
			timeline01.append( TweenMax.to(frames_mc, .5, {scaleX:1, scaleY:1, onComplete:playSubNav}) );							
			timeline01.append( TweenMax.to(bottom_bar_mc.mini_triangle_mc, .1, {alpha:.2, repeat:5, yoyo:true}) );				
			trace(seqNav.name);
			seqNav.visible=true;
			seqNav.gotoAndPlay(2);
			
			function playSubNav():void
			{
				subNavSelection.gotoAndPlay(2);
			}
			
		}
		
		private function navAnimationReverse():void
		{						
			var timeline02:TimelineMax = new TimelineMax({repeat:0});
			timeline02.append( TweenMax.to(main_nav_mc, .5, {autoAlpha:1}) );
			timeline02.append( TweenMax.to(frames_mc, .5, {scaleX:0, scaleY:0}) );	
			timeline02.append( TweenMax.to(corner_waves, .5, {scaleX:1, scaleY:1}) );
			timeline02.append( TweenMax.to(bottom_bar_mc.mini_triangle_mc, .1, {alpha:.2, repeat:5, yoyo:true}) );
		}

		//Setup and behavior for the Home button that brings the user back to the Main Nav
		private function flickHome():void
		{
			var bounds:Object = {left:234, right:476};
			//var currentX:Number = thumb.x;
			//var lastX:Number = thumb.x;
			//var vx:Number = 0;
			var isDragging:Boolean = false;
			var offset:Number;
			
			//addEventListener(Event.ENTER_FRAME, loop);					
			SUB_Location_mc.BTN_Home.addEventListener(MouseEvent.MOUSE_DOWN, onDown);
			SUB_Process_mc.BTN_Home.addEventListener(MouseEvent.MOUSE_DOWN, onDown);
			SUB_Gallery_mc.BTN_Home.addEventListener(MouseEvent.MOUSE_DOWN, onDown);			
			
			
			function onDown(e:MouseEvent):void
			{
					
				var myTarget = e.target.parent;
				trace(myTarget.name);				
				
				if(myTarget==SUB_Location_mc)
				{
					mySubNav = SUB_Location_mc;
				}
				if(myTarget==SUB_Process_mc)
				{
					mySubNav = SUB_Process_mc;
				}
				if(myTarget==SUB_Gallery_mc)
				{
					mySubNav = SUB_Gallery_mc;
				}
				
				mySubNav.addEventListener(MouseEvent.MOUSE_UP, onUp);				
				
				isDragging = true;
				offset = mySubNav.mouseX;
				trace(offset);
				addEventListener(MouseEvent.MOUSE_MOVE, onMove);					
				
			}
			
			
			function onUp(e:MouseEvent):void
			{				
				if(mySubNav.x<420)
				{
					var posOut = mySubNav.x - 20;
					trace(mySubNav.name);
					TweenMax.to(mySubNav, .5, {alpha:0, x:posOut});
					SUB_Gallery_mc.BTN_Home.branch = "index/nav/home";
					Gaia.api.goto(MovieClip(e.target).branch);					
					TweenMax.to(main_nav_mc, .5, {autoAlpha:1});
					TweenLite.to(seqNav, 1, {scaleX:0, scaleY:0, alpha:0, ease:Cubic.easeInOut, onComplete:resetSubNav});
					navAnimationReverse();						
					
					function resetSubNav():void
					{
						subNavSelection.gotoAndStop(1);
						trace(seqNav.name);
						seqNav.visible=false;
						TweenLite.to(seqNav, 0, {scaleX:1, scaleY:1, alpha:1});
						//nav_display_mc.seq_location.visible=false;
						//nav_display_mc.seq_process.visible=false;
						//nav_display_mc.seq_gallery.visible=false;
					}
					
				}
				else
				{
					TweenMax.to(mySubNav, .5, {x:476});					
				}
				
				isDragging = false;
				removeEventListener(MouseEvent.MOUSE_MOVE, onMove);
			}
			
			function onMove(e:MouseEvent):void
			{
				mySubNav.x = mouseX - offset;
				if(mySubNav.x <= bounds.left)
					mySubNav.x = bounds.left;
				else if(mySubNav.x >= bounds.right)
					mySubNav.x = bounds.right;
				e.updateAfterEvent();
				
				if(mouseY<560 || mouseY>598)
				{
					trace("out");
					TweenMax.to(mySubNav, .5, {x:476});
					isDragging = false;
					removeEventListener(MouseEvent.MOUSE_MOVE, onMove);
				}
				
				
				/*
				if(mouseY<560 || mouseY>598)
				{
					var posOut = mySubNav.x - 10;
					trace(mySubNav.name);
					TweenMax.to(mySubNav, .5, {alpha:0, x:posOut});
					SUB_Gallery_mc.BTN_Home.branch = "index/nav/home";
					Gaia.api.goto(MovieClip(e.target).branch);					
					main_nav_mc.visible=true;												
				}
				else
				{
					TweenMax.to(mySubNav, .5, {x:476});					
				}
				*/
				
			}
		}


		//Initialize Buttons and Behaviors
		public function initButtons():void
		{			
		
			flickHome();
		
			main_nav_mc.BTN_Location.branch = "index/nav/location/world";
				//SUB_Location_mc.BTN_Home.branch = "index/nav/home";
				SUB_Location_mc.locations_btns_mc.BTN_World.branch = "index/nav/location/world";
				SUB_Location_mc.locations_btns_mc.BTN_USA.branch = "index/nav/location/usa";
				SUB_Location_mc.locations_btns_mc.BTN_Portage.branch = "index/nav/location/portage";
				
			main_nav_mc.BTN_Process.branch = "index/nav/process/overview";				
				//SUB_Process_mc.BTN_Home.branch = "index/nav/home";
				SUB_Process_mc.process_btns_mc.BTN_Overview.branch = "index/nav/process/overview";
				SUB_Process_mc.process_btns_mc.BTN_Batch_House.branch = "index/nav/process/batch_house";
				SUB_Process_mc.process_btns_mc.BTN_Furnace.branch = "index/nav/process/furnace";
				SUB_Process_mc.process_btns_mc.BTN_Tin_Bath.branch = "index/nav/process/tin_bath";
				SUB_Process_mc.process_btns_mc.BTN_Annealing_Lehr.branch = "index/nav/process/annealing_lehr";				
				
			main_nav_mc.BTN_Gallery.branch = "index/nav/gallery/SPR_2009";
				//SUB_Gallery_mc.BTN_Home.branch = "index/nav/home";
				SUB_Gallery_mc.gallery_btns_mc.BTN_SPR_2009.branch = "index/nav/gallery/SPR_2009";
				SUB_Gallery_mc.gallery_btns_mc.BTN_FWR_2010.branch = "index/nav/gallery/FWR_2010";
				SUB_Gallery_mc.gallery_btns_mc.BTN_FR_2012.branch = "index/nav/gallery/FR_2012";
				SUB_Gallery_mc.gallery_btns_mc.BTN_Misc.branch = "index/nav/gallery/Misc";
				
			buttons = [main_nav_mc.BTN_Location, SUB_Location_mc.locations_btns_mc.BTN_World, SUB_Location_mc.locations_btns_mc.BTN_USA, SUB_Location_mc.locations_btns_mc.BTN_Portage, main_nav_mc.BTN_Process, SUB_Process_mc.process_btns_mc.BTN_Overview, SUB_Process_mc.process_btns_mc.BTN_Batch_House, SUB_Process_mc.process_btns_mc.BTN_Furnace, SUB_Process_mc.process_btns_mc.BTN_Tin_Bath, SUB_Process_mc.process_btns_mc.BTN_Annealing_Lehr, main_nav_mc.BTN_Gallery, SUB_Gallery_mc.gallery_btns_mc.BTN_SPR_2009, SUB_Gallery_mc.gallery_btns_mc.BTN_FWR_2010, SUB_Gallery_mc.gallery_btns_mc.BTN_FR_2012, SUB_Gallery_mc.gallery_btns_mc.BTN_Misc];
			var i:int = buttons.length;
			while (i--)
			{
				buttons[i].buttonMode = true;
				buttons[i].useHandCursor = false;
				buttons[i].mouseChildren = false;
				buttons[i].addEventListener(MouseEvent.CLICK, onClick);
			}
			Gaia.api.afterGoto(onAfterGoto);
			updateButtonStates(Gaia.api.getCurrentBranch());
		}
		private function onClick(event:MouseEvent):void
		{
			Gaia.api.goto(MovieClip(event.target).branch);
		}
		private function onAfterGoto(event:GaiaEvent):void
		{
			updateButtonStates(event.validBranch);
		}
		
		//Updates all button states that are set up to do so
		private function updateButtonStates(branch:String):void
		{
								
			var i:int = buttons.length;
			while (i--)
			{
				var btn:MovieClip = buttons[i];
				if (branch != btn.branch)
				{
					btn.gotoAndStop("_up");
					btn.enabled = true;									
				}
				else
				{
					btn.gotoAndStop("selected");					
					btn.enabled = false;					
					navCheck();					
				}
			}
			
			//Determine what to do based on nav selection
			function navCheck():void
			{
				if(btn.name=="BTN_Location")
				{
					trace("Location");
					resetNav();					
					//SUB_Location_mc.visible=true;					
					subNavSelection = SUB_Location_mc;
					seqNav = nav_display_mc.seq_location;
					navAnimation();
				}
				else if(btn.name=="BTN_Process")
				{
					trace("Process");
					resetNav();					
					//SUB_Process_mc.visible=true;					
					subNavSelection = SUB_Process_mc;
					seqNav = nav_display_mc.seq_process;
					navAnimation();
				}
				else if(btn.name=="BTN_Gallery")
				{
					trace("Gallery");
					resetNav();					
					//SUB_Gallery_mc.visible=true;									
					subNavSelection = SUB_Gallery_mc;
					seqNav = nav_display_mc.seq_gallery;
					navAnimation();
					
				}				
			}			
		}
		
		//Resets navigation to default settings
		private function resetNav():void
		{
			main_nav_mc.visible=true;
			SUB_Location_mc.visible=false;
			SUB_Process_mc.visible=false;
			SUB_Gallery_mc.visible=false;
			SUB_Location_mc.alpha=1;
			SUB_Process_mc.alpha=1;
			SUB_Gallery_mc.alpha=1;
			SUB_Location_mc.x = 476;
			SUB_Process_mc.x = 476;
			SUB_Gallery_mc.x = 476;
			
		}
		

	
		public function NavPage()
		{
			super();
			alpha = 0;
			initSettings();			
			initButtons();	
			//new Scaffold(this);
		}
		
		
		
		override public function transitionIn():void 
		{
			super.transitionIn();
			TweenMax.to(this, 0.3, {alpha:1, onComplete:transitionInComplete});
		}
		override public function transitionOut():void 
		{
			super.transitionOut();
			TweenMax.to(this, 0.3, {alpha:0, onComplete:transitionOutComplete});
		}
	}
}
