﻿package pages
{
	import com.gaiaframework.templates.AbstractPage;
	import com.gaiaframework.events.*;
	import com.gaiaframework.debug.*;
	import com.gaiaframework.api.*;
	import flash.display.*;
	import flash.events.*;
	import com.greensock.*;
	import com.greensock.easing.*;
	
	
	//import fl.video.*;
	import util.*;
	import flash.text.TextField;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	public class SPR_2009Page extends AbstractPage
	{	
		
		public var nextBtn:MovieClip;
		public var prevBtn:MovieClip;
		public var PageTxt:TextField;
		
		private var _objectHomeProps:Array;
		private var _photosListLoader:URLLoader;
		private var _albumTitleString:String = "album01";
		
		private var _photosList:Array;
		private var _photosListFiles:Array;
		private var _filesTotal:int;
		private var _photoList:Array;
		
		
		private var _pageCount:int = 1;
		private var _page:int = 1;
		
		public var lockStatus:String;
		public var sector:String;
		public var target01:MovieClip;
		public var target02:MovieClip;
		public var target03:MovieClip;
		public var target04:MovieClip;
		public var target05:MovieClip;
		public var thumbReset_mc:MovieClip;
		private var thumbs:Array;			
		
		//Vars for snglThrow()
		private var ms:MouseSpeed		= new MouseSpeed();
		private var xSpeed:Number		= 0;
		private var ySpeed:Number		= 0;
		private var friction:Number		= 0.96;  //0.96;
		private var offsetX:Number		= 0;
		private var offsetY:Number		= 0;
		private var myTarget = null;				
		
		public function initThumbs():void
		{			
			lockStatus="unlocked";	
			sector="none";
			
			targetAnimation();
			
			
			var i:int = _photoList.length;
			while (i--)
			{
				try{
					_photoList[i].width=108.5;
					_photoList[i].height=61.3;
					
					_photoList[i].doubleClickEnabled = true;
					_photoList[i].addEventListener( MouseEvent.DOUBLE_CLICK, dbleTap );				
					_photoList[i].addEventListener(TransformGestureEvent.GESTURE_ROTATE , TwoPtRotate);				
					_photoList[i].addEventListener(TransformGestureEvent.GESTURE_PAN , TwoPtPan);
					_photoList[i].addEventListener(TransformGestureEvent.GESTURE_ZOOM , pinchZoom);
					_photoList[i].addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);				
					
					
					
					if(i >= 16){
						_photoList[i].visible = false;
					}
				}catch(e:Error){
					//trace("e:Error ",e.name, e.message);
					continue;
				}
			}	             
			thumbReset_mc.addEventListener(MouseEvent.CLICK, thumbReset);
		}
		
		private function targetAnimation():void
		{					
			var timeline:TimelineMax = new TimelineMax({repeat:0});
			timeline.appendMultiple( TweenMax.allFrom([target01, target02, target03, target04, target05], .5, {delay:.5, scaleX:0, scaleY:0, autoAlpha:0, ease:Circ.easeIn}, .1), .6);
			timeline.appendMultiple( TweenMax.allFrom([target01, target02, target03, target04, target05], .5, {tint:0xffffff, yoyo:false}, .1), 0);
		}
		
		//Resets the thumbs to the positions set below
		private function thumbReset(event:MouseEvent):void
		{			
			xSpeed = 0;
			ySpeed = 0;		
			thumbReset_mc.gotoAndPlay(2);
			var startI:int = (_page*16)-16;
			var endI:int = ((_page+1)*16)-16;
			
			var photo:pages.GPhoto;
			var photosListLen:uint = _photosList.length;

			for(var i:int = 0; i < photosListLen; i++){
				photo = _photoList[i];
			
				try{
					if(i >= startI && i < endI){
					TweenMax.to(photo, .5, {x:photo.originalX, y:photo.originalY, width:photo.originalWidth, height:photo.originalHeight, rotation:0});
				}
					
				}catch(e:Error){
					continue;
				}
			}
	}
		

		
		public function pinchZoom(e:TransformGestureEvent):void
		{			
			e.target.scaleX *= (e.scaleX+e.scaleX)/2; 
			e.target.scaleY *= (e.scaleX+e.scaleY)/2; 
		}
		
		public function TwoPtPan(e:TransformGestureEvent):void
		{					
			e.target.y += e.offsetY;
			e.target.x += e.offsetX;
		}
		
		public function TwoPtRotate(e:TransformGestureEvent):void
		{			
			e.target.rotation += e.rotation;
		}
		
		public function dbleTap(event:MouseEvent):void
		{
			TweenMax.to(event.target, 1, {x:960, y:540, width:108.5, height:61.3, rotation:0});
		}
		
		
		//This starts the throw functionality
		private function mouseDownHandler(e:MouseEvent):void
		{			
			myTarget = e.currentTarget;
			var myTargetName = myTarget.name;				
			
			myTarget.addEventListener(Event.ENTER_FRAME, throwobject);				

			setChildIndex(myTarget, numChildren-1);
			stage.addEventListener(Event.ENTER_FRAME, drag);
			stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
			offsetX = MovieClip(e.currentTarget).mouseX;
			offsetY = MovieClip(e.currentTarget).mouseY;
					
		}
			
		private function mouseUpHandler(e:MouseEvent):void
		{		
			stage.removeEventListener(Event.ENTER_FRAME, drag);
			stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
			xSpeed = ms.getXSpeed();
			ySpeed = ms.getYSpeed();					
		}
			
		/*
		The offset x and y variables are just to remember where on the box you clicked.
		Without it, when the "MOUSE_DOWN" is registered there would be a little jump as 
		the box moved to the exact mouseX and mouseY coordinates.
		*/
			
			
		private function drag(e:Event):void
		{	
			myTarget.x = mouseX;// - offsetX;
			myTarget.y = mouseY;// - offsetY;
		}			

			
		//Throws object with momentum based on the friction setting	
		public function throwobject(e:Event)
		{							
			// move object and apply friction
			myTarget.x += xSpeed;
			myTarget.y += ySpeed;
				
			xSpeed *= friction;
			ySpeed *= friction;
				
			// keep object within bounds (stage borders)
			if(myTarget.x > 1920-(myTarget.width/2))
			{
				//trace("Stage w:" + stage.stageWidth + "|| Target w:" + e.target.width);
				myTarget.x = 1920-(myTarget.width/2);
				xSpeed *= -1;
			}
			if(myTarget.x < 640+(myTarget.width/2))
			{
				myTarget.x = 640+(myTarget.width/2);
				xSpeed *= -1;
			}
			if(myTarget.y > 1080-(myTarget.height/2))
			{
				myTarget.y = 1080-(myTarget.height/2);
				ySpeed *= -1;
			}
			if(myTarget.y < 0+(myTarget.height/2))
			{
				myTarget.y = 0+(myTarget.height/2);
				ySpeed *= -1;
			}
				
			//Checks to see which sector the thumbnail is in and then locks and expands image to fit
			function sectorCheck():void
			{								
				if(myTarget.x>1430 && myTarget.x<1770 && myTarget.y>84 && myTarget.y<276)
				{						
					sector="upperRight";				
					myTarget.x = 1600;
					myTarget.y = 180;	
				}
				else if(myTarget.x>1430 && myTarget.x<1770 && myTarget.y>804 && myTarget.y<996)
				{						
					sector="lowerRight";				
					myTarget.x = 1600;
					myTarget.y = 900;	
				}
				else if(myTarget.x>1430 && myTarget.x<1770 && myTarget.y>444 && myTarget.y<636)
				{	
					sector="Right";				
					myTarget.x = 1600;
					myTarget.y = 540;						
				}				
				else if(myTarget.x>790 && myTarget.x<1130 && myTarget.y>84 && myTarget.y<276)
				{						
					sector="Top";				
					myTarget.x = 960;
					myTarget.y = 180;	
				}				
				else if(myTarget.x>790 && myTarget.x<1130 && myTarget.y>804 && myTarget.y<996)
				{						
					sector="Bottom";				
					myTarget.x = 960;
					myTarget.y = 900;	
				}
				else
				{
					sector="none";
				}
					
			}
				
			if(lockStatus=="unlocked")
			{		
				sectorCheck();					
				if(sector=="upperRight" || sector=="lowerRight" || sector=="Right" || sector=="Bottom" || sector=="Top")					
				{											
					TweenMax.to(myTarget, .5, {width:640, height:360, rotation:0});						
					lockStatus="locked";						
					trace("locked");																	
				}
				
			}				
			else if (lockStatus=="locked")
			{	
				sectorCheck();
				if (sector=="none")
				{									
					TweenMax.to(myTarget, 1, {width:108.5, height:61.3, rotation:0});
					lockStatus="unlocked";
					trace("unlocked");
				}
																									
			}			
				
		}
		
		//Change the friction by editing the var at the top of the page
		private function changeFriction(e:Event):void
		{
			friction = e.target.value;				
		}						

		
		public function SPR_2009Page()
		{
			super();
			alpha = 0;			
			
			_photoList = new Array();
			_photosListFiles = new Array();
			_objectHomeProps = new Array();
			
			_objectHomeProps.push({x:782, y:433 , width:108.5, height:61.3, rotation:0});
			_objectHomeProps.push({x:899, y:433 , width:108.5, height:61.3, rotation:0});
			_objectHomeProps.push({x:1016, y:433 , width:108.5, height:61.3, rotation:0});
			_objectHomeProps.push({x:1134, y:433 , width:108.5, height:61.3, rotation:0});
			_objectHomeProps.push({x:782, y:503 , width:108.5, height:61.3, rotation:0});
			_objectHomeProps.push({x:899, y:503 , width:108.5, height:61.3, rotation:0});
			_objectHomeProps.push({x:1016, y:503 , width:108.5, height:61.3, rotation:0});
			_objectHomeProps.push({x:1134, y:503 , width:108.5, height:61.3, rotation:0});
			_objectHomeProps.push({x:782, y:574 , width:108.5, height:61.3, rotation:0});
			_objectHomeProps.push({x:899, y:574 , width:108.5, height:61.3, rotation:0});
			_objectHomeProps.push({x:1016, y:574 , width:108.5, height:61.3, rotation:0});
			_objectHomeProps.push({x:1134, y:574 , width:108.5, height:61.3, rotation:0});
			_objectHomeProps.push({x:782, y:644 , width:108.5, height:61.3, rotation:0});
			_objectHomeProps.push({x:899, y:644 , width:108.5, height:61.3, rotation:0});
			_objectHomeProps.push({x:1016, y:644 , width:108.5, height:61.3, rotation:0});
			_objectHomeProps.push({x:1134, y:644 , width:108.5, height:61.3, rotation:0});
			
			//load in txt file
			_photosListLoader = new URLLoader();
			_photosListLoader.addEventListener(Event.COMPLETE, onPhotosListLoaded);
			_photosListLoader.load(new URLRequest("library/albums/" + _albumTitleString  + "/photoslist.txt"));
			
			//new Scaffold(this);
			
			nextBtn.addEventListener(MouseEvent.CLICK, onNextPage);
			prevBtn.addEventListener(MouseEvent.CLICK, onPrevPage);
			
			
		}
		
		private function onPhotosListLoaded(e:Event):void{
			trace("@@@onPhotosListLoaded()");

			_photosList = String(_photosListLoader.data).split(",");
			trace("_photosList.length ",_photosList.length);
			trace(">>>_photosList=",_photosList,"<<<");
			

			
			var photosListLen:uint = _photosList.length;
			
			_pageCount = Math.ceil(photosListLen/16);
			trace("_pageCount ",_pageCount);
			
			PageTxt.text = _page +"/"+ _pageCount;
			
			_filesTotal = photosListLen;
			var i2:int = 0;
			
			for(var i:int = 0; i < _filesTotal; i++){
				
				
				//try{	
					var fileLoader:Loader = new Loader();
					
					//TODO remove this querystring? may prevent caching?
					var imageURL:URLRequest = new URLRequest("library/albums/" + _albumTitleString + "/" + _photosList[i]+"?index="+i);
					trace("imageURL=",imageURL.url);
					
					configureListeners(fileLoader.contentLoaderInfo);
					trace("t1");
					_photosListFiles.push(fileLoader);
					trace("t2");
					fileLoader.load(imageURL);		
					//FileManager.addToQueue("library/albums/" + _albumTitle.text + "/" + _photosList[fileCount]);
					trace("photosPath ","library/albums/" + _albumTitleString + "/" + _photosList[i]+"?index="+i);
				/*}catch(e:Error){
					continue;
				}
			*/
				
				
				trace("i=",i);
				trace("@1");
				
				
				
				var photo:pages.GPhoto = new pages.GPhoto();
				//trace("@14");
				//
				photo.name = "photo"+i;
				
				if(i2 <= 15){
					trace("i2 ",i2);
					photo.x = photo.originalX = _objectHomeProps[i2].x;
					photo.y = photo.originalY = _objectHomeProps[i2].y;
					photo.rotation = photo.originalRotation = _objectHomeProps[i2].rotation;
					photo.width = photo.originalWidth = _objectHomeProps[i2].width;
					photo.height = photo.originalHeight = _objectHomeProps[i2].height;
					
					i2==15 ? i2=0 : i2++;
				}
				//
				////photo.addChild(bmp);
				//trace("@18.5");
				//
				_photoList.push(photo);
				addChild(photo);
				//
				//
				//trace("@19");
				//
			}
			//inst pics, set home props
			//
			initThumbs();
			
		}
		
		private function configureListeners(dispatcher:IEventDispatcher):void {
			
            dispatcher.addEventListener(Event.COMPLETE, completeHandler);
            dispatcher.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler);
            dispatcher.addEventListener(Event.INIT, initHandler);
            dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
            dispatcher.addEventListener(Event.OPEN, openHandler);
            dispatcher.addEventListener(ProgressEvent.PROGRESS, progressHandler);
            dispatcher.addEventListener(Event.UNLOAD, unLoadHandler);
        }
		
		private function completeHandler(event:Event):void {
			trace("completeHandler()");
			
			var bmpLoader:Loader = Loader(event.target.loader);
            var bmpInfo:LoaderInfo = LoaderInfo(bmpLoader.contentLoaderInfo);
		
			
			trace("$$$index",bmpInfo.url.split("?index=")[1]);
			//+"?index="+i
            //trace("completeHandler: name =" + bmpLoader.name);
			
			var bmp:Loader = bmpLoader;
				Bitmap(bmp.content).smoothing = true;
				
				trace("@1.1");
				trace(".type",Object(bmp.content).constructor);
				
				trace("@1.2");
				trace(".Image", bmp.name);
				trace("@1.3");
				trace(".type",Object(bmp.content).constructor);
				trace("@1.4");
				//trace(".Image", bmp.content.loaderInfo.url);
				
				//trace("bmp", bmp.content.loaderInfo.url.toString());
				trace("@2");
				trace("url ",bmp.contentLoaderInfo.url);
				bmp.name = bmp.contentLoaderInfo.url;
				trace("@3");
				var scaleFactor:Number;
				trace("@3.5");

				trace("###bmp.width ",bmp.width);
				trace("###bmp.height ",bmp.height);
				
				var index:* = bmpInfo.url.split("?index=")[1];
				
				trace("index=*",index+"*");
				
				//CFGPhoto(_photoList[index])._originalWidth = bmp.width;
				//CFGPhoto(_photoList[index])._originalHeight = bmp.height;
				//CFGPhoto(_photoList[index])._originalMatrix = bmp.transform.matrix;
				

				
				
				if(bmp.width > bmp.height && bmp.width > 108.5){
					
					trace("@4");
					scaleFactor = 108.5 / bmp.width;
					trace("@5");
					bmp.scaleX = scaleFactor;
					trace("@6");
					bmp.scaleY = scaleFactor;
					trace("@7");
				
				}else if(bmp.height > bmp.width && bmp.height > 108.5){
					
					trace("@8");
					scaleFactor = 108.5 / bmp.height;
					trace("@9");
					bmp.scaleX = scaleFactor;
					trace("@10");
					bmp.scaleY = scaleFactor;
					trace("@11");

				}

				trace('index = ' + index + '_photoList = ' + _photoList );
				var photo:pages.GPhoto = pages.GPhoto( _photoList[index]);
				photo.hideDefImage();
				
				//photo._scaleFactor = scaleFactor;
				bmp.x = -(bmp.width/2);
				bmp.y = -(bmp.height/2);
				
				photo.addChild(bmp);
				try{
					//trace("defaultImg = ",photo.getDefImg());
					
					//photo.getChildByName("defaultImg").visible = false;
					//photo.getDefImg().visible = false;
					
				}catch(e:Error){
					trace("Error ",e.name, e.message);
				}
				
				
				
				trace("###bmp.scaleX ",bmp.scaleX);
				trace("###bmp.scaleY ",bmp.scaleY);
				
				trace("###bmp.width ",bmp.width);
				trace("###bmp.height ",bmp.height);
				
				trace("@12");
				
				trace("Image", bmp.content.loaderInfo.url);
				trace("Image", bmp.name);
				trace("type",Object(bmp.content).constructor);
				trace("@13");
				
				
				
				
				trace("@14");
        }

        private function httpStatusHandler(event:HTTPStatusEvent):void {
            //trace("httpStatusHandler: " + event);
        }

        private function initHandler(event:Event):void {
            //trace("initHandler: " + event);
        }

        private function ioErrorHandler(event:IOErrorEvent):void {
            //trace("ioErrorHandler: " + event);
        }

        private function openHandler(event:Event):void {
            //trace("openHandler: " + event);
        }

        private function progressHandler(event:ProgressEvent):void {
            trace("progressHandler: bytesLoaded= " + event.bytesLoaded + " bytesTotal=" + event.bytesTotal);
        }

        private function unLoadHandler(event:Event):void {
            //trace("unLoadHandler: " + event);
        }

		private function onNextPage(e:MouseEvent):void{
			trace("onNextPage()");
			
			if(_page < _pageCount){
				_page++;
				PageTxt.text = _page +"/"+ _pageCount;
				
				var startI:int = (_page*16)-16;
				var endI:int = ((_page+1)*16)-16;
				var photosListLen:uint = _photosList.length;
				
				for(var i:int = 0; i < photosListLen; i++){
				
					_photoList[i].visible = false;
					
						if(i >= startI && i < endI){
							_photoList[i].visible = true;
						}else{
							_photoList[i].visible = false;
						}
				}
				
			}
		}
		
		private function onPrevPage(e:MouseEvent):void{
			trace("onPrevPage()");
			
			if(_page > 1){
				_page--;
				PageTxt.text = _page +"/"+ _pageCount;
				
				var startI:int = (_page*16)-16;
				var endI:int = ((_page+1)*16)-16;
				var photosListLen:uint = _photosList.length;

				for(var i:int = 0; i < photosListLen; i++){
					
					_photoList[i].visible = false;
					
						if(i >= startI && i < endI){
							_photoList[i].visible = true;
						}else{
							_photoList[i].visible = false;
						}
				}
			}
		}
		
		override public function transitionIn():void 
		{
			super.transitionIn();
			TweenMax.to(this, 0.3, {alpha:1, onComplete:transitionInComplete});
		}
		override public function transitionOut():void 
		{
			super.transitionOut();
			TweenMax.to(this, 0.3, {alpha:0, onComplete:transitionOutComplete});
		}
	}
}
