﻿package pages
{
	import com.gaiaframework.templates.AbstractPage;
	import com.gaiaframework.events.*;
	import com.gaiaframework.debug.*;
	import com.gaiaframework.api.*;
	import flash.display.*;
	import flash.events.*;
	import com.greensock.TweenMax;
	import com.greensock.TweenLite;
	
	//import fl.video.*;
	import util.*;
	
	public class SPR_2009Page extends AbstractPage
	{	
		
		public var object01:MovieClip;
		public var object02:MovieClip;
		public var object03:MovieClip;
		public var object04:MovieClip;
		public var lockStatus:String;
		public var sector:String;
		public var target01:MovieClip;
		public var target02:MovieClip;
		public var thumbReset_mc:MovieClip;
		private var thumbs:Array;	
		
		//Vars for snglThrow()
		private var ms:MouseSpeed		= new MouseSpeed();
		private var xSpeed:Number		= 0;
		private var ySpeed:Number		= 0;
		private var friction:Number		= 0.85;  //0.96;
		private var offsetX:Number		= 0;
		private var offsetY:Number		= 0;
		private var myTarget = null;				
		
		public function initThumbs():void
		{
			object01.width=200;
			object01.height=112.5;
			object02.width=200;
			object02.height=112.5;
			object03.width=200;
			object03.height=112.5;
			object04.width=200;
			object04.height=112.5;
			
			thumbs = [object01, object02, object03, object04];
			var i:int = thumbs.length;
			while (i--)
			{
				trace(thumbs[i]);
				thumbs[i].doubleClickEnabled = true;
				thumbs[i].addEventListener( MouseEvent.DOUBLE_CLICK, dbleTap );				
				thumbs[i].addEventListener(TransformGestureEvent.GESTURE_ROTATE , TwoPtRotate);				
				thumbs[i].addEventListener(TransformGestureEvent.GESTURE_PAN , TwoPtPan);
				thumbs[i].addEventListener(TransformGestureEvent.GESTURE_ZOOM , pinchZoom);
				thumbs[i].addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
				
				thumbReset_mc.addEventListener(MouseEvent.CLICK, thumbReset);
			}	
		}
		
		
		private function thumbReset(event:MouseEvent):void
		{
			thumbReset_mc.gotoAndPlay(2);
			TweenLite.to(object01, .5, {x:150, y:469 , scaleX:2, scaleY:2, rotation:0});
			TweenLite.to(object02, .5, {x:150, y:609 , scaleX:2, scaleY:2, rotation:0});
			TweenLite.to(object03, .5, {x:372, y:469 , scaleX:2, scaleY:2, rotation:0});
			TweenLite.to(object04, .5, {x:372, y:609 , scaleX:2, scaleY:2, rotation:0});				
		}
		
		
		public function initDualTouch():void
		{					
			initThumbs();
			
			lockStatus="unlocked";	
			sector="none";
			
			//pinchZoom();
			//TwoPtPan();
			//TwoPtRotate();
			//dbleTap();
			snglThrow();								
			
		}		
		
		public function pinchZoom(e:TransformGestureEvent):void
		{			
			e.target.scaleX *= (e.scaleX+e.scaleY)/2; 
			e.target.scaleY *= (e.scaleX+e.scaleY)/2; 
		}
		
		public function TwoPtPan(e:TransformGestureEvent):void
		{					
			e.target.y += e.offsetY;
			e.target.x += e.offsetX;
		}
		
		public function TwoPtRotate(e:TransformGestureEvent):void
		{			
			e.target.rotation += e.rotation;
		}
		
		public function dbleTap(event:MouseEvent):void
		{
			TweenLite.to(event.target, 1, {x:960, y:540, scaleX:2, scaleY:2, rotation:0});
		}
		
			private function mouseDownHandler(e:MouseEvent):void
			{			
				myTarget = e.currentTarget;
				var myTargetName = myTarget.name;				
				
				myTarget.addEventListener(Event.ENTER_FRAME, throwobject);				

				setChildIndex(myTarget, numChildren-1);
				stage.addEventListener(Event.ENTER_FRAME, drag);
				stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
				offsetX = mouseX - e.target.x;
				offsetY = mouseY - e.target.y;		
			}
			
			function mouseUpHandler(e:MouseEvent):void
			{		
				stage.removeEventListener(Event.ENTER_FRAME, drag);
				stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
				xSpeed = ms.getXSpeed();
				ySpeed = ms.getYSpeed();					
			}
			
			/*
			The offset x and y variables are just to remember where on the box you clicked.
			Without it, when the "MOUSE_DOWN" is registered there would be a little jump as 
			the box moved to the exact mouseX and mouseY coordinates.
			*/
			
			
			function drag(e:Event):void
			{	
				myTarget.x = mouseX - offsetX;
				myTarget.y = mouseY - offsetY;
			}			

			
			
			function throwobject(e:Event)
			{							
				// move object and apply friction
				myTarget.x += xSpeed;
				myTarget.y += ySpeed;
				
				xSpeed *= friction;
				ySpeed *= friction;
				
				// keep object within bounds (stage borders)
				if(myTarget.x > stage.stageWidth)
				{
					//trace("Stage w:" + stage.stageWidth + "|| Target w:" + e.target.width);
					myTarget.x = stage.stageWidth;
					xSpeed *= -1;
				}
				if(myTarget.x < 0)
				{
					myTarget.x = 0;
					xSpeed *= -1;
				}
				if(myTarget.y > stage.stageHeight)
				{
					myTarget.y = stage.stageHeight;
					ySpeed *= -1;
				}
				if(myTarget.y < 0)
				{
					myTarget.y = 0;
					ySpeed *= -1;
				}
				
								
				function sectorCheck():void
				{								
					if(myTarget.x>1280 && myTarget.x<1920 && myTarget.y>0 && myTarget.y<360)
					{						
						sector="upperRight";				
						myTarget.x = 1600;
						myTarget.y = 180;	
					}
					else if(myTarget.x>1280 && myTarget.x<1920 && myTarget.y>720 && myTarget.y<1080)
					{						
						sector="lowerRight";				
						myTarget.x = 1600;
						myTarget.y = 900;	
					}
					else
					{
						sector="none";
					}
					
				}
				
				if(lockStatus=="unlocked")
				{		
					sectorCheck();					
					if(sector=="upperRight" || sector=="lowerRight")					
					{											
						TweenLite.to(myTarget, .5, {scaleX:6.5, scaleY:6.5, rotation:0});						
						lockStatus="locked";						
						trace("locked");																	
					}
	
				}				
				else if (lockStatus=="locked")
				{	
					sectorCheck();
					if (sector=="none")
					{									
						TweenLite.to(myTarget, 1, {scaleX:2, scaleY:2, rotation:0});
						lockStatus="unlocked";
						trace("unlocked");
					}
																									
				}			
				
			}
			
			function changeFriction(e:Event):void
			{
				friction = e.target.value;				
			}			
			
		
			
		
		public function snglThrow():void
		{	
			/*
			var ms:MouseSpeed		= new MouseSpeed();
			var xSpeed:Number		= 0;
			var ySpeed:Number		= 0;
			var friction:Number		= 0.85;  //0.96;
			var offsetX:Number		= 0;
			var offsetY:Number		= 0;
			var myTarget = null;						
			*/
			
			/*
			object01.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);			
			object02.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler); 			
			object03.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);			
			object04.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);			
			*/
			/*
			function mouseDownHandler(e:MouseEvent):void
			{			
				myTarget = e.currentTarget;
				var myTargetName = myTarget.name;				
				
				myTarget.addEventListener(Event.ENTER_FRAME, throwobject);				

				setChildIndex(myTarget, numChildren-1);
				stage.addEventListener(Event.ENTER_FRAME, drag);
				stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
				offsetX = mouseX - e.target.x;
				offsetY = mouseY - e.target.y;		
			}
			
			function mouseUpHandler(e:MouseEvent):void
			{		
				stage.removeEventListener(Event.ENTER_FRAME, drag);
				stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
				xSpeed = ms.getXSpeed();
				ySpeed = ms.getYSpeed();					
			}
			
			/*
			The offset x and y variables are just to remember where on the box you clicked.
			Without it, when the "MOUSE_DOWN" is registered there would be a little jump as 
			the box moved to the exact mouseX and mouseY coordinates.
			*/
			
			
			function drag(e:Event):void
			{	
				myTarget.x = mouseX - offsetX;
				myTarget.y = mouseY - offsetY;
			}			

			
			
			function throwobject(e:Event)
			{							
				// move object and apply friction
				myTarget.x += xSpeed;
				myTarget.y += ySpeed;
				
				xSpeed *= friction;
				ySpeed *= friction;
				
				// keep object within bounds (stage borders)
				if(myTarget.x > stage.stageWidth)
				{
					//trace("Stage w:" + stage.stageWidth + "|| Target w:" + e.target.width);
					myTarget.x = stage.stageWidth;
					xSpeed *= -1;
				}
				if(myTarget.x < 0)
				{
					myTarget.x = 0;
					xSpeed *= -1;
				}
				if(myTarget.y > stage.stageHeight)
				{
					myTarget.y = stage.stageHeight;
					ySpeed *= -1;
				}
				if(myTarget.y < 0)
				{
					myTarget.y = 0;
					ySpeed *= -1;
				}
				
								
				function sectorCheck():void
				{								
					if(myTarget.x>1280 && myTarget.x<1920 && myTarget.y>0 && myTarget.y<360)
					{						
						sector="upperRight";				
						myTarget.x = 1600;
						myTarget.y = 180;	
					}
					else if(myTarget.x>1280 && myTarget.x<1920 && myTarget.y>720 && myTarget.y<1080)
					{						
						sector="lowerRight";				
						myTarget.x = 1600;
						myTarget.y = 900;	
					}
					else
					{
						sector="none";
					}
					
				}
				
				if(lockStatus=="unlocked")
				{		
					sectorCheck();					
					if(sector=="upperRight" || sector=="lowerRight")					
					{											
						TweenLite.to(myTarget, .5, {scaleX:6.5, scaleY:6.5, rotation:0});						
						lockStatus="locked";						
						trace("locked");																	
					}
	
				}				
				else if (lockStatus=="locked")
				{	
					sectorCheck();
					if (sector=="none")
					{									
						TweenLite.to(myTarget, 1, {scaleX:2, scaleY:2, rotation:0});
						lockStatus="unlocked";
						trace("unlocked");
					}
																									
				}			
				
			}
			
			function changeFriction(e:Event):void
			{
				friction = e.target.value;				
			}			
			*/
		}
			
		
		public function SPR_2009Page()
		{
			super();
			alpha = 0;			
			initDualTouch();
			//new Scaffold(this);
		}
		override public function transitionIn():void 
		{
			super.transitionIn();
			TweenMax.to(this, 0.3, {alpha:1, onComplete:transitionInComplete});
		}
		override public function transitionOut():void 
		{
			super.transitionOut();
			TweenMax.to(this, 0.3, {alpha:0, onComplete:transitionOutComplete});
		}
	}
}
