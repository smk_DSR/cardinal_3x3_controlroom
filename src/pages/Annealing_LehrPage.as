﻿package pages
{
	import com.gaiaframework.templates.AbstractPage;
	import com.gaiaframework.events.*;
	import com.gaiaframework.debug.*;
	import com.gaiaframework.api.*;
	import fl.video.*;
	import flash.display.*;
	import flash.events.*;
	import flash.net.URLLoader;
    import flash.net.URLRequest;	
	import com.greensock.*;
	import com.greensock.easing.*;
	
	public class Annealing_LehrPage extends AbstractPage
	{	
	
		private var buttons:Array;		
		public var display_mc:MovieClip;
		public var BTN_img01:MovieClip;
		public var BTN_img02:MovieClip;
		public var BTN_img03:MovieClip;
		public var BTN_img04:MovieClip;
		public var BTN_img05:MovieClip;
		public var BTN_img06:MovieClip;
		public var BTN_vid01:MovieClip;
		public var BTN_vid02:MovieClip;
		public var BTN_vid03:MovieClip;
		
		public var myXML:XML;		
		
		//Loads the XML file
		public function loadXML():void 
		{								
			
			var myLoader:URLLoader = new URLLoader();
			myLoader.load(new URLRequest("gallery/annealing_lehr/annealing_lehr.xml"));
			
			
			myLoader.addEventListener(Event.COMPLETE, processXML);
			
			function processXML(e:Event):void 
			{
				myXML = new XML(e.target.data);
				/*
				for (var i:int = 0; i<myXML.*.length(); i++)
				{
					//trace(myXML.ITEM.MEDIA[i]);
					//trace(myXML.ITEM.CAPTION[i]);
				}
				*/
			}
		}		
		//Initialize the FLV Player
		public function initFLVPlayer():void
		{			
			//---- Keeps the FLV Component from acting stupid ----//
			display_mc.frame_mc.myPlayer.fullScreenTakeOver=false;
			display_mc.frame_mc.myPlayer.addEventListener(VideoEvent.COMPLETE, onFLVComplete);
			
			function onFLVComplete(event:VideoEvent):void {
				  event.target.play();	  
			}
			
			//--------- Player Setup ---------//
			display_mc.frame_mc.myPlayer.volume = .2;
			display_mc.frame_mc.myPlayer.visible=false;
			display_mc.frame_mc.image_loader_mc.visible=false;
			display_mc.frame_mc.myPlayer.stop();
		}
		
		//Initialize the button array and button states
		public function initButtons():void
		{
			loadXML();			
			buttons = [display_mc.BTN_img01, display_mc.BTN_img02, display_mc.BTN_img03, display_mc.BTN_img04, display_mc.BTN_img05, display_mc.BTN_img06, display_mc.BTN_vid01, display_mc.BTN_vid02, display_mc.BTN_vid03];
			var i:int = buttons.length;
			while (i--)
			{
				buttons[i].buttonMode = true;				
				buttons[i].mouseChildren = false;
				buttons[i].useHandCursor = false;
				buttons[i].addEventListener(MouseEvent.CLICK, onClick);					
			}
			
			display_mc.caption_mc.visible=false;
			
			//Disabled buttons because they have no media associated																
				buttons[2].mouseEnabled = false;
				buttons[3].mouseEnabled = false;
				buttons[4].mouseEnabled = false;
				buttons[5].mouseEnabled = false;
				//buttons[8].mouseEnabled = false;
				
		}
		
		private function onClick(event:MouseEvent):void
		{
			var i:int = buttons.length;
			while (i--)
			{							
				var btn:MovieClip = buttons[i];
				if(event.target==btn)
				{
					btn.gotoAndStop("selected");
					btn.enabled = false;					
					
					var myIMGLoader:Loader = new Loader();	
					var myTXTLoader = myXML.ITEM.CAPTION[i];
					var imgRequest:URLRequest = new URLRequest(myXML.ITEM.MEDIA[i]);
					
					myIMGLoader.load(imgRequest);	
					display_mc.frame_mc.image_loader_mc.addChild(myIMGLoader);			
					
					switch (btn.name)
					{
						case "BTN_img01":																
						videoOff();						
						break;
						
						case "BTN_img02":															
						videoOff();
						break;	
						
						case "BTN_img03":													
						videoOff();						
						break;	
						
						case "BTN_img04":														
						videoOff();						
						break;	
						
						case "BTN_img05":	
						videoOff();						
						break;	
						
						case "BTN_img06":							
						videoOff();						
						break;	
						
						case "BTN_vid01":								
						display_mc.frame_mc.myPlayer.source = "gallery/annealing_lehr/annealing_lehr_01.f4v";	
						videoOn(true);
						break;
						
						case "BTN_vid02":								
						display_mc.frame_mc.myPlayer.source = "gallery/annealing_lehr/annealing_lehr_02.mov";	
						videoOn(false);
						break;	
						
						case "BTN_vid03":								
						display_mc.frame_mc.myPlayer.source = "gallery/annealing_lehr/LIVE_ANNEALING_LEHR.mov";	
						videoOn(false);
						break;	
					}
					
					//Swaps the FLV Player for image loader
					function videoOff():void
					{
						display_mc.frame_mc.myPlayer.stop();
						display_mc.frame_mc.myPlayer.visible=false;											
						display_mc.frame_mc.image_loader_mc.visible=true;						
						initCaption();						
					}
					//Swaps the image loader for FLV Player
					function videoOn(caption:Boolean = true):void
					{
						
						display_mc.frame_mc.myPlayer.visible=true;
						display_mc.frame_mc.image_loader_mc.visible=false;
						display_mc.frame_mc.myPlayer.autoRewind=true;						
						display_mc.frame_mc.myPlayer.play();
						
						if(caption){
							initCaption();
						}else{
							TweenMax.to(display_mc.caption_mc, .5, {alpha:0, y:27, ease:Circ.easeInOut});							
						}
					}	
					//Loads and animates the caption text box
					function initCaption():void
					{
						if(display_mc.caption_mc.visible==true)
						{
							trace("hi");																										
							TweenMax.to(display_mc.caption_mc, .5, {alpha:0, y:27, ease:Circ.easeInOut, onComplete:loadText});							
							
							
							function loadText():void
							{								
								display_mc.caption_mc.y = 139;
								display_mc.caption_mc.alpha=1;
								display_mc.caption_mc.caption_txt.text = myTXTLoader;
								TweenMax.from(display_mc.caption_mc, .5, {alpha:0, scaleX:0, scaleY:0, ease:Circ.easeInOut});
							}
						}
						else
						{
							display_mc.caption_mc.visible=true;
							display_mc.caption_mc.caption_txt.text = myTXTLoader;													
							TweenMax.from(display_mc.caption_mc, .5, {alpha:0, scaleX:0, scaleY:0, ease:Circ.easeInOut});						
						}
					}
				}
				else
				{
					btn.gotoAndStop("_up");
					btn.enabled = true;
				}

			}			
			
		}
		
		
		//Display animation in
		public function initDisplay():void
		{
			var timeline:TimelineMax = new TimelineMax({repeat:0});			
			timeline.append(TweenMax.from(display_mc, .5, {delay:.5, y:670, alpha:0}));			
			
			timeline.appendMultiple( TweenMax.allFrom([display_mc.view_frames_left, 
												  		 display_mc.view_frames_right], .25, 
														 {scaleX:0, alpha:0, ease:Cubic.easeInOut}, 0));
			timeline.appendMultiple( TweenMax.allFrom([display_mc.view_frames_left, 
												  		 display_mc.view_frames_right], .5, 
														 {tint:0xFFFFFF}, 0));
			timeline.appendMultiple( TweenMax.allFrom([display_mc.left_view_mc, 
												  		 display_mc.right_view_mc], 1, 
														 {alpha:0}, 0));
			timeline.append(TweenMax.from(display_mc.title_mc, .5, {alpha:0}));	
		}
	
		public function Annealing_LehrPage()
		{
			super();
			alpha = 0;
			initDisplay();
			initButtons();
			initFLVPlayer();
			//new Scaffold(this);
		}
		override public function transitionIn():void 
		{
			super.transitionIn();
			TweenMax.to(this, 0.3, {alpha:1, onComplete:transitionInComplete});
		}
		override public function transitionOut():void 
		{
			super.transitionOut();
			TweenMax.to(this, 0.3, {alpha:0, onComplete:transitionOutComplete});
		}
	}
}
