﻿package pages{
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.display.Stage;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.geom.Point;
	
	
	public class LocationDetailsWindow extends MovieClip {
		
		private var _parentScope:MovieClip;
		private var _pf:PortageFeatures;
		public var CloseBtn:MovieClip;
		public var BodyTxt:TextField;
		public var HeaderTxt:TextField;
		public var PlantImages:MovieClip;
		
		private var _pdw:PortageDirectionsWindow;
		private var _pfpw:PortageFloorPlanWindow;
		
		private var bText:TextField;
		private var hText:TextField;
		private var _globalCenterPoint:Point;
		
		public function LocationDetailsWindow(scope:MovieClip){
			// constructor code
			
			_parentScope = scope
			
			_globalCenterPoint = new Point(760,200);

			CloseBtn.addEventListener(MouseEvent.CLICK, onMouseClick);
			
			bText = BodyTxt;
			hText = HeaderTxt;
			
		}

		public function setHeader(text:String, size:int):void{
			var tformat:TextFormat = new TextFormat();
			tformat.size = size;
			
			hText.text = text;
			hText.setTextFormat(tformat);
		}
		

		public function setBody(text:String, size:int):void{
			var tformat:TextFormat = new TextFormat();
			tformat.size = size;
			
			bText.text = text;
			bText.setTextFormat(tformat);
		}
		
		private function onMouseClick(event:MouseEvent):void{
				
			_parentScope.closeLocationDetailsWindow();
			
		}
		
		private function onExpandClick(event:MouseEvent):void{
			pfpwClose(null);
			_pdw = new PortageDirectionsWindow();
			_pdw.x = globalToLocal(_globalCenterPoint).x;
			_pdw.y = globalToLocal(_globalCenterPoint).y;
			_pdw.DirectionsCloseBtn.addEventListener(MouseEvent.CLICK, pdwClose);
			_pdw.DirectionsMinimizeBtn.addEventListener(MouseEvent.CLICK, pdwMinimize);
			_pdw.DirectionsCompassBtn.addEventListener(MouseEvent.CLICK, onDirections2Click);
			this.addChildAt(_pdw, this.numChildren);
			
		}
		
		
		private function onDirectionsClick(event:MouseEvent):void{
			
			_pfpw = new PortageFloorPlanWindow();
			_pfpw.x = globalToLocal(_globalCenterPoint).x;
			_pfpw.y = globalToLocal(_globalCenterPoint).y;
			
			_pfpw.FloorPlanCloseBtn.addEventListener(MouseEvent.CLICK, pfpwClose);
			_pfpw.FloorPlanMinimizeBtn.addEventListener(MouseEvent.CLICK, pfpwMinimize);
			//_pfpw.FloorPlanCompassBtn.addEventListener(MouseEvent.CLICK, pfpwCompass);
			this.addChildAt(_pfpw, this.numChildren);
			
			
			
		}
		
		private function onDirections2Click(event:MouseEvent):void{
			
			_pfpw = new PortageFloorPlanWindow();
			_pfpw.x = globalToLocal(_globalCenterPoint).x;
			_pfpw.y = globalToLocal(_globalCenterPoint).y;
			
			_pfpw.FloorPlanCloseBtn.addEventListener(MouseEvent.CLICK, pfpw2Close);
			_pfpw.FloorPlanMinimizeBtn.addEventListener(MouseEvent.CLICK, pfpw2Minimize);
			_pdw.DirectionsCompassBtn.removeEventListener(MouseEvent.CLICK, onDirections2Click);
			//_pfpw.FloorPlanCompassBtn.addEventListener(MouseEvent.CLICK, pfpwCompass);
			this.addChildAt(_pfpw, this.numChildren);
			
		}

		public function pdwClose(event:MouseEvent):void{
			
			try{
				this.removeChild(_pdw);
				_parentScope.closeLocationDetailsWindow();
				_pdw.DirectionsCloseBtn.RemoveListener(MouseEvent.CLICK, pdwClose);
				_pdw.DirectionsMinimizeBtn.RemoveListener(MouseEvent.CLICK, pdwMinimize);
				
			}catch(e:Error){
				//Ignore
			}
			
		}
		
		public function pdw2Close(event:MouseEvent):void{
			
			try{
				this.removeChild(_pdw);
				_parentScope.closeLocationDetailsWindow();
				_pdw.DirectionsCloseBtn.RemoveListener(MouseEvent.CLICK, pdwClose);
				_pdw.DirectionsMinimizeBtn.RemoveListener(MouseEvent.CLICK, pdwMinimize);
				
			}catch(e:Error){
				//Ignore
			}
			
		}
		
		
		private function pdwMinimize(event:MouseEvent):void{
			try{
				this.removeChild(_pdw);
				_pdw.DirectionsCloseBtn.addRemoveListener(MouseEvent.CLICK, pdwClose);
				_pdw.DirectionsMinimizeBtn.addRemoveListener(MouseEvent.CLICK, pdwMinimize);
				
			}catch(e:Error){
				//Ignore
			}
			
				
		}
		
		public function pfpwClose(event:MouseEvent):void{
			
			try{
				this.removeChild(_pfpw);
				_parentScope.closeLocationDetailsWindow();
				_pfpw.FloorPlanCloseBtn.removeEventListener(MouseEvent.CLICK, pfpwClose);
				_pfpw.FloorPlanMinimizeBtn.removeEventListener(MouseEvent.CLICK, pfpwMinimize);
			}catch(e:Error){
				//Ignore
			}
			
		}
		
		public function pfpw2Close(event:MouseEvent):void{
			
			try{
				this.removeChild(_pfpw);
				_pfpw.FloorPlanCloseBtn.removeEventListener(MouseEvent.CLICK, pfpw2Close);
				_pfpw.FloorPlanMinimizeBtn.removeEventListener(MouseEvent.CLICK, pfpw2Minimize);
			}catch(e:Error){
				//Ignore
			}
			pdw2Close(event);
		}

		private function pfpwMinimize(event:MouseEvent):void{
			try{
				this.removeChild(_pfpw);
				_pfpw.FloorPlanCloseBtn.removeEventListener(MouseEvent.CLICK, pfpwClose);
				_pfpw.FloorPlanMinimizeBtn.removeEventListener(MouseEvent.CLICK, pfpwMinimize);
				
			}catch(e:Error){
				//Ignore
			}
			
		}
		
		private function pfpw2Minimize(event:MouseEvent):void{
			
			try{
				this.removeChild(_pfpw);
				_pfpw.FloorPlanCloseBtn.removeEventListener(MouseEvent.CLICK, pfpw2Close);
				_pfpw.FloorPlanMinimizeBtn.removeEventListener(MouseEvent.CLICK, pfpw2Minimize);
				
			}catch(e:Error){
				//Ignore
			}
			pdwMinimize(event);
		}
		

		
		public function checkFeatures():void{
			trace("checkFeatures()", hText.text);
		
			if(hText.text == "PORTAGE FG"){
				trace("Match");
				
				if(!_pf){
					_pf = new PortageFeatures();
					_pf.x = 318;
					_pf.y = 115;
					_pf.ExpandBtn.addEventListener(MouseEvent.CLICK, onExpandClick);
					_pf.DirectionsBtn.addEventListener(MouseEvent.CLICK, onDirectionsClick);
					this.addChild(_pf);
				}
				
			}else{
				
				try{
					if(_pf){
						_pf.ExpandBtn.removeEventListener(MouseEvent.CLICK, onExpandClick);
						_pf.DirectionsBtn.removeEventListener(MouseEvent.CLICK, onDirectionsClick);
						this.removeChild(_pf);
						_pf = null;
					}
				}catch(e:Error){
				
					//Ignore
				}
			

				
			}
			
		}
		
		public function killMe():void{
			trace("killMe()");
			
			trace("show1");
			pfpwClose(null);
			trace("show2");
			pfpw2Close(null);
			trace("show3");
			pdwClose(null);
			trace("show4");
			pdw2Close(null);
			trace("show5");
			
		}
		
	}	
}
